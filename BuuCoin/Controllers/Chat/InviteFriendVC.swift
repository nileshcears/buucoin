//
//  InviteFriendVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/09/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class InviteFriendVC: UIViewController {

    var pageMenu : CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPageMenu()
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension InviteFriendVC {
    func setPageMenu() {
        var arrController : [UIViewController] = []
        
        let vcBuy = loadVC(strStoryboardId: SB_CHAT, strVCId: idLocalSearchVC) as! LocalSearchVC
        vcBuy.title = "Local Search"
        arrController.append(vcBuy)
        
        let vcSell = loadVC(strStoryboardId: SB_CHAT, strVCId: idGlobalSearchVC) as! GlobalSearchVC
        vcSell.title = "Global Search"
        arrController.append(vcSell)
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(Color_Hex(hex: "1DAEF9")),
            .bottomMenuHairlineColor(Color_Hex(hex: "9296B4")),
            .selectedMenuItemLabelColor(UIColor.white),
            .unselectedMenuItemLabelColor(Color_Hex(hex: "9296B4")),
            .selectionIndicatorHeight(2),
            .menuHeight(54),
            .menuMargin(0),
            .menuItemFont(UIFont(name: "Montserrat-Light", size: 15)!),
            .menuItemWidth(self.view.frame.size.width/CGFloat(arrController.count)),
            .centerMenuItems(true)
        ]
        
        let frame = CGRect(x: 0, y: 74, width: self.view.frame.size.width, height: self.view.frame.size.height-74)
        
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        pageMenu!.controllerScrollView.isScrollEnabled = false
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
    }
}
