//
//  GlobalSearchVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/09/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class GlobalSearchVC: UIViewController {
    @IBOutlet var tblUserList: UITableView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var viewNDF: UIView!
    
    var arrUserList: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNDF.alpha = 0
    }

    @IBAction func btnSearch(_ sender: Any) {
        if TRIM(string: txtSearch.text!).count > 0 {
            sendSearchUserReq()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GlobalSearchVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewNDF.alpha = 1
        if arrUserList.count > 0 {
            viewNDF.alpha = 0
        }
        return arrUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UserListCell = tableView.dequeueReusableCell(withIdentifier: "UserListCell", for: indexPath) as! UserListCell
        let dictUser: NSDictionary = arrUserList.object(at: indexPath.row) as! NSDictionary
        cell.lblName.text = dictUser.object(forKey: "loginname") as? String
        cell.lblEmail.text = dictUser.object(forKey: "loginemail") as? String
        cell.lblFirstLetter.text = ""
        if cell.lblName.text?.count > 0 {
            cell.lblFirstLetter.text = cell.lblName.text!.prefix(1).capitalized
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictUser: NSDictionary = arrUserList.object(at: indexPath.row) as! NSDictionary
        
        let vc : ChatVC = loadVC(strStoryboardId: SB_CHAT, strVCId: idChatVC) as! ChatVC
        vc.selectedUser = dictUser
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
}

extension GlobalSearchVC {
    func sendSearchUserReq() {
        self.view.endEditing(true)
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let dictParam : NSDictionary = NSDictionary()
            let strURL = String(format:"%@/searchuser?email=%@",BASE_URL,TRIM(string: txtSearch.text!))
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if dictResponse["success"] as? String == "1" {
                    self.arrUserList = (arrayOfFilteredBy(arr: dictResponse.object(forKey: "data") as! NSArray)).mutableCopy() as! NSMutableArray
                }
                self.tblUserList.reloadData()
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}
