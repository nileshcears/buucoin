//
//  PayRequestVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit
import Canvas

let kMaxRadius: CGFloat = 300
let kMaxDuration: TimeInterval = 10

class PayRequestVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var img1: UIImageView!
    @IBOutlet var img2: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var txtComment: UITextField!
    @IBOutlet var btnPayReq: UIButton!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var viewSuccess: CSAnimationView!
    @IBOutlet var viewAmntY: NSLayoutConstraint!
    @IBOutlet var viewCmntBottom: NSLayoutConstraint!
    
    var isPay: Bool = false
    let pulsator = Pulsator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtAmount.delegate = self
        viewSuccess.alpha = 0
        if isPay {
            lblTitle.text = "Paying to John Deo"
            btnPayReq.setImage(UIImage(named:"btn_paynow"), for: .normal)
        }
        if self.view.frame.size.height <= 568 {
            viewAmntY.constant = 30
            viewCmntBottom.constant = 50
        }
        
        btnDone.layer.superlayer?.insertSublayer(pulsator, below: btnDone.layer)
        pulsator.numPulse = 5
        pulsator.radius = 0.7 * kMaxRadius
        pulsator.animationDuration = 0.5 * kMaxDuration
        pulsator.backgroundColor = UIColor.white.cgColor
        pulsator.start()
    }

    override func viewDidLayoutSubviews() {
        pulsator.position = btnDone.layer.position
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayReq(_ sender: UIButton) {
        viewSuccess.alpha = 1
        viewSuccess.startCanvasAnimation()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let dotsCount = textField.text!.components(separatedBy: ".").count - 1
        if dotsCount > 0 && (string == "." || string == ",") {
            return false
        }
        if string == "," {
            textField.text! += "."
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
