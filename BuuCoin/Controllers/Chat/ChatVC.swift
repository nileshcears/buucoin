//
//  ChatVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftyJSON

class ChatSendCell: UITableViewCell {
    @IBOutlet weak var lblMessage: UILabel!
}

class ChatReceiveCell: UITableViewCell {
    @IBOutlet weak var lblMessage: UILabel!
}

class RequestCell: UITableViewCell {
    
}

class ChatVC: UIViewController {
    
    //MARK:- Outlet
    var arrData = NSMutableArray()
    @IBOutlet var txtMsg: IQTextView!
    @IBOutlet var btnChatTop: UIButton!
    @IBOutlet var xPayReq: NSLayoutConstraint!
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblFirstLetter: UILabel!
    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var btnPay: UIButton!
    @IBOutlet var btnReq: UIButton!
    @IBOutlet var viewPayReq: UIView!
    
    @IBOutlet weak var tblChat: UITableView!
    //MARK:- Variable
    var selectedUser : NSDictionary =  NSDictionary.init()
    var arrMessage = NSMutableArray()
    var senderJID:String = ""
    var senderFName:String = ""
    var senderLName:String = ""
    var senderProfilePath:String = ""
    
    var receiverJID:String = ""
    var receiverFName:String = ""
    var receiverLName:String = ""
    var receiverImage:String = ""
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        receiverJID = "\(selectedUser.value(forKey: "loginid") ?? "")"
        receiverFName = "\(selectedUser.value(forKey: "loginname") ?? "")"
        
        let dictUserData = UserDefaults.standard.object(forKey: LOGIN_USER_DATA) as! NSDictionary
        senderJID =  "\(dictUserData.value(forKey: "loginid") ?? "")"
        senderFName = "\(dictUserData.value(forKey: "loginname") ?? "")"
        APP_DELEGATE.currentChatUserID = senderJID
        
        DBManager.shared.getAllMessaget(strSenderID: senderJID, strReceiverID: receiverJID) { (isComplete, strMessage, arrMessage) in
            self.arrMessage = NSMutableArray.init(array: arrMessage!)
            tblChat.delegate = self
            tblChat.dataSource = self
            tblChat.reloadData()
            
            if(self.arrMessage.count > 0)
            {
                tblChat.alpha = 0.0
                UIView.animate(withDuration: 0.2, animations: {
                    self.tblChat.alpha = 1.0
                    self.scrollToBottom(animated: false)
                })
            }
        }
        
        self.fill_Header()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //SUBMIT NEW MESSAGE OBSERVER
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNewMessage(notification:)), name: Notification.Name("ReceivedNewMessage"), object: nil)
    }
    
    @objc func getNewMessage(notification: Notification){
        let objLastMessage  = arrMessage.lastObject as! LDMessage
        DBManager.shared.getAllNewMessaget(strSenderID: senderJID, strReceiverID: receiverJID, lastMessageID: "\(objLastMessage.messageId!)") { (isComplete, strMessage, arrMessage) in
            for objMessage in arrMessage as! [LDMessage]
            {
                self.arrMessage.add(objMessage)
                self.tblChat.beginUpdates()
                self.tblChat.insertRows(at: [IndexPath.init(row: self.arrMessage.count - 1, section: 0)], with: .none)
                self.tblChat.endUpdates()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.scrollToBottom(animated: true)
            }
        }
    }
    
    
    //MARK:-
    func fill_Header() -> Void {
        
        var strImgName : String = "#"
        var strName : String = ""
        
        if selectedUser.allKeys.count > 0 {
            strName = selectedUser.value(forKey: "loginname") as! String
            strImgName = strName.prefix(1).capitalized
        }
        
        self.lblFirstLetter.text = strImgName
        self.lblName.text = strName
    }
    
    //MARK: Table View Scrolling
    func scrollToBottom(animated:Bool = false){
        if(self.arrMessage.count > 0)
        {
            DispatchQueue.main.async {
                let indexPath = IndexPath(
                    row: self.arrMessage.count - 1 ,
                    section: 0)
                self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
        }
    }
    
    func scrollToTop(animated:Bool = false) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.tblChat.scrollToRow(at: indexPath, at: .top, animated: animated)
        }
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMore(_ sender: Any) {
    }
    
    @IBAction func btnPay(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_CHAT, strVCId: idPayRequestVC) as! PayRequestVC
        vc.isPay = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnRequest(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_CHAT, strVCId: idPayRequestVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCloseChat(_ sender: Any) {
        btnChatTop(UIButton())
    }
    
    @IBAction func btnChatTop(_ sender: Any) {
        if self.xPayReq.constant == 10 {
            UIView.animate(withDuration: 0.3, animations: {
                self.btnChatTop.isHidden = true
                self.xPayReq.constant = -118
                self.view.layoutIfNeeded()
                self.viewPayReq.isHidden = true
            }, completion: { (finished) in
                self.txtMsg.becomeFirstResponder()
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.txtMsg.resignFirstResponder()
                self.viewPayReq.isHidden = false
                self.xPayReq.constant = 10
                self.btnChatTop.isHidden = false
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
            })
        }
    }
    
    @IBAction func btnSendMessAction(_ sender: Any) {
        if TRIM(string: txtMsg.text) != "" {
            self.sendTextMessage(strMessage: txtMsg.text!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:-
extension ChatVC
{
    func sendTextMessage(strMessage:String)
    {
        let dicMessage:NSDictionary = [
            Sender_Firstname:senderFName,
            Sender_Lastname:senderLName,
            Sender_Profiel_Pic:senderProfilePath,
            Receiver_Firstname:receiverFName ,
            Receiver_Lastname:receiverLName ,
            Receiver_Profiel_Pic:receiverImage,
            Message_ID:Int(NSDate().timeIntervalSince1970),
            Sender_ID:Int(senderJID) ?? 0,
            Receiver_ID:Int(receiverJID) ?? 0,
            Message_Type:MSG_TYPE_TEXT,
            Message:strMessage,
            Media:"",
            Latitude:"",
            Longitude:"",
            Contact_ID:"",
            Contact_Pic:"",
            Contact_Number:"",
            Read_Status:"0",
            Deleted:"0",
            Created_Date:DateFormater.getTimestampUTC()]
        
        self.txtMsg.text = ""
        self.addNewMessage(dicMessage: dicMessage)
        
        DBManager.shared.insertNewTextMessage(dicMessage: dicMessage)
        APP_DELEGATE.xmppController.sendMessage(dicMessage: dicMessage, strReceiverJabbarID: "\(receiverJID)")
    }
    
    func addNewMessage(dicMessage:NSDictionary,dicMeeting:NSDictionary = [:])
    {
        let jsonData = JSON(dicMessage)
        let objMessage:LDMessage = LDMessage.init(json: jsonData)
        self.arrMessage.add(objMessage)
        self.tblChat
            .beginUpdates()
        self.tblChat.insertRows(at: [IndexPath.init(row: self.arrMessage.count - 1, section: 0)], with: .none)
        self.tblChat.endUpdates()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.scrollToBottom(animated: true)
        }
    }
}


extension ChatVC {
    func getChatDataReq() {
        /*self.view.endEditing(true)
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let dictParam : NSDictionary = NSDictionary()
            let strURL = String(format:"%@/searchuser?email=%@",BASE_URL,TRIM(string: txtSearch.text!))
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if dictResponse["success"] as? String == "1" {
                    self.arrUserList = (arrayOfFilteredBy(arr: dictResponse.object(forKey: "data") as! NSArray)).mutableCopy() as! NSMutableArray
                }
                self.tblUserList.reloadData()
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }*/
    }
}

extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objCurrentMessage = arrMessage[indexPath.row] as! LDMessage
        switch objCurrentMessage.messagetype ?? 0 {
        case 0:
            if("\(objCurrentMessage.senderId ?? 0)" == senderJID)
            {
                return self.loadSendCell(indexPath: indexPath)
            }
            else
            {
                return self.loadReceiveCell(indexPath: indexPath)
            }
        default:
            if("\(objCurrentMessage.senderId ?? 0)" == senderJID)
            {
                return self.loadSendCell(indexPath: indexPath)
            }
            else
            {
                return self.loadReceiveCell(indexPath: indexPath)
            }
        }
    }
    
    func  loadReceiveCell(indexPath:IndexPath) -> ChatReceiveCell {
        let objCurrentMessage = arrMessage[indexPath.row] as! LDMessage
        let cellReceive:ChatReceiveCell = tblChat.dequeueReusableCell(withIdentifier: "ChatReceiveCell") as! ChatReceiveCell
        cellReceive.lblMessage.text = objCurrentMessage.message ?? ""
      
        cellReceive.selectionStyle = .none
        return cellReceive
    }
    
    func  loadSendCell(indexPath:IndexPath) -> ChatSendCell {
        let objCurrentMessage = arrMessage[indexPath.row] as! LDMessage
        let cellSend:ChatSendCell = tblChat.dequeueReusableCell(withIdentifier: "ChatSendCell") as! ChatSendCell
        cellSend.lblMessage.text = objCurrentMessage.message ?? ""
     
        cellSend.selectionStyle = .none
        return cellSend
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
