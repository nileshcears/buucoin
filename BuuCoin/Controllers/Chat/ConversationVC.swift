//
//  ConversationVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var lblTime: UILabel!
    
    @IBOutlet weak var viewUnread: UIView!
    @IBOutlet var lblUnread: UILabel!
}

class ConversationVC: UIViewController {

    @IBOutlet var tblConv: UITableView!
    @IBOutlet weak var viewEmptyConv: UIView!
    
    var objChatList:LDChatList!
    var arrConvList: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        APP_DELEGATE.isChatListScreen = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.getUpdateChatList(notification:)), name: Notification.Name("UpdateChatList"), object: nil)
        self.getAllChatList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        APP_DELEGATE.isChatListScreen = false
        NotificationCenter.default.removeObserver(self, name: Notification.Name("UpdateChatList"), object: nil)
        self.view.endEditing(true)
    }

    @objc func getUpdateChatList(notification: Notification){
        DBManager.shared.getAllChatList { (isComplete, strMessage, arrChatList) in
            if(isComplete == true)
            {
                if(arrChatList?.count > 0)
                {
                    self.arrConvList = NSMutableArray.init(array: arrChatList!)
                    self.tblConv.reloadData()
                }
            }
        }
    }
    
    func getAllChatList()
    {
        DBManager.shared.getAllChatList { (isComplete, strMessage, arrChatList) in
            if(isComplete == true)
            {
                if(arrChatList?.count > 0)
                {
                    self.arrConvList = NSMutableArray.init(array: arrChatList!)
                    self.tblConv.reloadData()
                }
            }
        }
    }
    

    @IBAction func btnMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_CHAT, strVCId: idInviteFriendVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ConversationVC {
    func getChatListReq() {
        self.view.endEditing(true)
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "Getting your chat users")
            
            strToken = GetDeviceToken()
            let dictParam : NSDictionary = NSDictionary()
            let strURL = String(format:"%@/chatlist",BASE_URL)
            
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if dictResponse["success"] as? String == "1" {
                    self.arrConvList = (arrayOfFilteredBy(arr: dictResponse.object(forKey: "data") as! NSArray)).mutableCopy() as! NSMutableArray
                }
                //self.tblConv.reloadData()
                
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}

extension ConversationVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let noOfConv : Int = self.arrConvList.count
        
        self.tblConv.isHidden = true
        self.viewEmptyConv.isHidden = true
        if noOfConv == 0 { self.viewEmptyConv.isHidden = false }
        else { self.tblConv.isHidden = false }
        
        return noOfConv
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return loadDiscussionCell(indexPath: indexPath)
    }
    
    func  loadDiscussionCell(indexPath:IndexPath) -> ConversationCell {
         let cell : ConversationCell = tblConv.dequeueReusableCell(withIdentifier: "ConversationCell", for: indexPath) as! ConversationCell
        objChatList = arrConvList[indexPath.row] as! LDChatList
        
        cell.lblName.text = "\(objChatList.firstname ?? "") \(objChatList.lastname ?? "")"
        cell.lblMsg.text =  "\(objChatList.lastmessage ?? "")"
        cell.lblTime.text = DateFormater.convertDateToLocalTimeZone(givenDate: objChatList.lastmessageTime!, fromFormat: GLOBAL_DATE_FORMAT, toFormat: APPDISPLAY_TIME)
        
         cell.selectionStyle = .none
         return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let objCurrentChatList = arrConvList[indexPath.row] as! LDChatList
         let strReceiverID = "\(UserDefaults.standard.object(forKey: UserID) ?? "")" == "\(objCurrentChatList.chatUserId!)" ?  "\(objCurrentChatList.loginUserId!)" : "\(objCurrentChatList.chatUserId!)"
        let dictUser: NSDictionary = ["loginid":strReceiverID,"loginname":"\(objCurrentChatList.firstname ?? "")"]
        let vc : ChatVC = loadVC(strStoryboardId: SB_CHAT, strVCId: idChatVC) as! ChatVC
        vc.selectedUser = dictUser
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
