//
//  LocalSearchVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/09/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit
import Contacts
import CoreTelephony
import MessageUI

class UserListCell: UITableViewCell {
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var btnInvite: UIButton!
    @IBOutlet var lblFirstLetter: UILabel!
}

class LocalSearchVC: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var tblUserList: UITableView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var viewNDF: UIView!
    
    var is_searching: Bool = false
    var arrSearchUser: NSMutableArray = NSMutableArray()
    var arrEmails: NSMutableArray = NSMutableArray()
    var arrUserList: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNDF.alpha = 0
        
        txtSearch.addTarget(self, action: #selector(LocalSearchVC.getSearchText(_:)), for: UIControlEvents.editingChanged)
        
        Contacts.fetchContacts { (result) in
            switch result {
                
            case .success(let contacts):
                for i in 0..<contacts.count {
                    let contact = contacts[i]
                    let strEmail: String = contact.emailAddresses.first?.value as String? ?? ""
                    let strName: String = contact.givenName as String? ?? ""
                    if strEmail.count > 0 {
                        let dict = ["loginname":strName,
                                    "loginemail":strEmail]
                        self.arrEmails.add(dict)
                    }
                }
                break
            case .error(let error):
                Log("------ error")
                let alert = UIAlertController(style: .alert, title: "Error", message: error.localizedDescription)
                alert.addAction(title: "OK") { [unowned self] action in
                    self.alertController?.dismiss(animated: true)
                }
                alert.show()
            }
        }
        sendInviteStatusReq()
    }
    
    @objc func getSearchText(_ textField : UITextField){
        if TRIM(string: txtSearch.text!).count > 0 {
            is_searching = true
            arrSearchUser.removeAllObjects()
            for index in 0 ..< arrUserList.count
            {
                let dictData = arrUserList.object(at: index) as! NSDictionary
                let loginName = dictData.value(forKey: "loginname") as? String
                let loginEmail = dictData.value(forKey: "loginemail") as? String
                
                if loginName!.range(of: txtSearch.text!, options:.regularExpression) != nil || loginEmail!.range(of: txtSearch.text!, options:.regularExpression) != nil {
                    arrSearchUser.add(dictData)
                }
            }
            self.tblUserList.reloadData()
        } else {
            self.view.endEditing(true)
            is_searching = false
            arrSearchUser.removeAllObjects()
            tblUserList.reloadData()
        }
        
    }

    @IBAction func btnSearch(_ sender: Any) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension LocalSearchVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_searching {
            viewNDF.alpha = arrSearchUser.count > 0 ? 0 : 1
            return arrSearchUser.count
        } else {
            viewNDF.alpha = arrUserList.count > 0 ? 0 : 1
            return arrUserList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UserListCell = tableView.dequeueReusableCell(withIdentifier: "UserListCell", for: indexPath) as! UserListCell
        var dictUser: NSDictionary = NSDictionary()
        if is_searching {
            dictUser = arrSearchUser.object(at: indexPath.row) as! NSDictionary
        } else {
            dictUser = arrUserList.object(at: indexPath.row) as! NSDictionary
        }
        cell.lblName.text = dictUser.object(forKey: "loginname") as? String
        cell.lblEmail.text = dictUser.object(forKey: "loginemail") as? String ?? ""
        
        cell.btnInvite.isHidden = true
        let strInvite = dictUser.object(forKey: "invite") as? String ?? "0"
        if strInvite == "1" {
            cell.btnInvite.isHidden = false
            cell.btnInvite.addTarget(self, action: #selector(btnInvite(_:)), for: .touchUpInside)
        }
        cell.lblFirstLetter.text = ""
        if cell.lblName.text?.count > 0 {
            cell.lblFirstLetter.text = cell.lblName.text!.prefix(1).capitalized
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    @objc func btnInvite(_ sender: UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblUserList)
        let indexPath: NSIndexPath = tblUserList.indexPathForRow(at: buttonPosition)! as NSIndexPath
        let dict: NSDictionary = arrUserList.object(at: indexPath.row) as! NSDictionary
        
        let strEmail = dict.object(forKey: "loginemail") as! String
        var strEmailBody = ""
        
        if (UserDefaults.standard.object(forKey: "balancedetail") != nil) {
            let dictBalance = UserDefaults.standard.object(forKey: "balancedetail") as! NSDictionary
            strEmailBody = dictBalance.object(forKey: "mail_body") as? String ?? "0"
        }
        
        if MFMailComposeViewController.canSendMail() {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients([strEmail])
            composeVC.setSubject("Invite to BuuCoin")
            composeVC.setMessageBody(strEmailBody, isHTML: true)
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension LocalSearchVC {
    func sendInviteStatusReq() {
        self.view.endEditing(true)
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let arremail = arrEmails.value(forKeyPath: "loginemail") as! NSArray
            let dictParam : NSDictionary = ["emails":arremail]
            let strURL = String(format:"%@/invitestatus",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                
                if dictResponse["success"] as? String == "1" {
                    self.arrUserList = (arrayOfFilteredBy(arr: dictResponse.object(forKey: "data") as! NSArray)).mutableCopy() as! NSMutableArray
                }
            
                for i in 0..<self.arrEmails.count {
                    let dict = self.arrEmails.object(at: i) as! NSDictionary
                    let strEmail = dict.object(forKey: "loginemail") as! String
                    let arr = self.arrUserList.filtered(using: NSPredicate(format: "loginemail = %@", strEmail)) as NSArray
                    if arr.count <= 0 {
                        
                        let dictTmp = ["blockunblock":"0",
                                       "loginemail":strEmail,
                                       "loginid":"0",
                                       "loginname":dict.object(forKey: "loginname") as! String,
                                       "walletaddress":"",
                                       "invite":"1"]
                        self.arrUserList.add(dictTmp)
                    }
                }
    
                self.tblUserList.reloadData()
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}
