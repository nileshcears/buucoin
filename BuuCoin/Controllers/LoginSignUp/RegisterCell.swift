//
//  RegisterCell.swift
//  Hi-TEK
//
//  Created by Admin on 03/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class RegisterCell: UITableViewCell {

    @IBOutlet var btnScanPairingCode: UIButton!
    @IBOutlet var btnCantAccessCam: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
