//
//  LoginVC.swift
//  Hi-TEK
//
//  Created by Admin on 03/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class LoginVC: UIViewController, verifyDelegate {

    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var loginBtnY: NSLayoutConstraint!
    @IBOutlet var viewQrBottom: NSLayoutConstraint!
    
    var isFromPairWallet: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.view.frame.size.height <= 568 {
            loginBtnY.constant = 25
            viewQrBottom.constant = 10
        }
        
        //txtEmail.text = "malkesh@mailinator.com"
        //txtPassword.text = "test1234"
        txtEmail.text = "android10@mailinator.com" //For get chat conv. list
        txtPassword.text = "12345678"
    }

    // MARK: - Button Click
    @IBAction func btnRegister(_ sender: Any) {
        self.view.endEditing(true)
        let vc = loadVC(strStoryboardId: SB_LS, strVCId: idRegisterVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPairWallet(_ sender: Any) {
        self.view.endEditing(true)
        if isFromPairWallet {
            self.navigationController?.popViewController(animated: true)
        } else {
            let vc = loadVC(strStoryboardId: SB_LS, strVCId: idPairWallet)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        if validateTxtFieldLength(txtEmail, withMessage: EnterEmail) &&
            validateEmailAddress(txtEmail, withMessage: EnterValidEmail) &&
            validateTxtFieldLength(txtPassword, withMessage: EnterPassword) &&
            validateMinTxtFieldLength(txtPassword, withMessage: EnterMinLengthPassword) {
            self.view.endEditing(true)
            sendLoginReq()
        }
    }
    
    //MARK:- All API
    func sendLoginReq()
    {
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let dictParam : NSDictionary = ["loginemail" : txtEmail.text!,
                                            "loginpassword" : txtPassword.text!.aesEncrypt(),
                                            "platform" : PLATFORM,
                                            "device_token" : GetPushToken()]
        
            let strURL = String(format:"%@/buulogin",BASE_URL)
            
            let manager = AFHTTPRequestOperationManager()
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if (dictResponse.object(forKey: "token") != nil) {
                    let token = (dictResponse.object(forKey: "token") as! String).aesDecrypt()
                    strToken = token.aesEncrypt()
                    storeInUserDefault(strToken, forKey: LOGIN_TOKEN)
                }
                if dictResponse["success"] as? String == "1" {
                    self.saveUserData(dictResponse: dictResponse)
                } else if dictResponse["success"] as? String == "2" && dictResponse.object(forKey: "message") as! String == "Please check your email for verification." {
                    let vc : Verify = Verify(nibName: "Verify", bundle: nil)
                    vc.verifyDelegate = self
                    self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
                }
                else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
    
    func closeVerifyPopUp(dictResponse: NSDictionary) {
        storeInUserDefault("1", forKey: IS_2FAAUTH)
        str2FAauthActive = "1"
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
        
        if (dictResponse.object(forKey: "token") != nil) {
            let token = (dictResponse.object(forKey: "token") as! String).aesDecrypt()
            strToken = token.aesEncrypt()
            storeInUserDefault(strToken, forKey: LOGIN_TOKEN)
        }
        
        let when = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.saveUserData(dictResponse: dictResponse)
        }
    }
    
    func closePopUp() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
    
    func saveUserData(dictResponse: NSDictionary) {
        let arrData : NSArray = dictResponse.object(forKey: "data") as! NSArray
        if arrData.count > 0 {
            let dictData = dictionaryOfFilteredBy(dict: arrData.object(at: 0) as! NSDictionary)
            storeInUserDefault(dictData, forKey: LOGIN_USER_DATA)
            dictUserData = dictData
            storeInUserDefault(dictUserData.value(forKey: "loginid"), forKey: UserID)
        }
        storeInUserDefault("1", forKey: IS_LOGIN)
        
        APP_DELEGATE.connectToChat()
        let vc = loadVC(strStoryboardId: SB_LS, strVCId: idPasscode)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
