//
//  RegisterVC.swift
//  Hi-TEK
//
//  Created by Admin on 03/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
    
    @IBOutlet var txtFName: UITextField!
    @IBOutlet var txtEMail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtConfirmPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnRegister(_ sender: Any) {

        if validateTxtFieldLength(txtFName, withMessage: EnterFullname) &&
            validateFullTxtFieldLength(txtFName, withMessage: EnterMinLengthName) &&
            validateTxtFieldLength(txtEMail, withMessage: EnterEmail) &&
            validateEmailAddress(txtEMail, withMessage: EnterValidEmail) &&
            validateTxtFieldLength(txtPassword, withMessage: EnterPassword) &&
            validateMinTxtFieldLength(txtPassword, withMessage: EnterMinLengthPassword) &&
            validateTxtFieldLength(txtConfirmPass, withMessage: EnterConfirmPassword) &&
            passwordMismatch(txtPassword, txtConfirmPass, withMessage: PasswordMismatch) {
            self.view.endEditing(true)
            let dictParam : NSDictionary = ["loginname" : txtFName.text!,
                                            "secretpin" : "",
                                            "loginemail" : txtEMail.text!,
                                            "loginpassword" : txtPassword.text!.aesEncrypt(),
                                            "confloginpassword" : txtConfirmPass.text!.aesEncrypt(),
                                            "platform" : PLATFORM,
                                            "device_token" : GetPushToken()]
            sendSignupReq(dictParam: dictParam)
        }
    }
    
    //MARK:- Button Method
    @IBAction func btnLogin(_ sender: Any) {
        self.view.endEditing(true)
        let vc = loadVC(strStoryboardId: SB_LS, strVCId: idLoginVC)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension RegisterVC {
    func sendSignupReq(dictParam : NSDictionary)
    {
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let strURL = String(format:"%@/signup",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if dictResponse["success"] as? String == "1" {
                    let alertView = UIAlertController(title: App_Title, message: dictResponse.object(forKey: "message") as? String, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        let vc = loadVC(strStoryboardId: SB_LS, strVCId: idLoginVC)
                        self.navigationController?.pushViewController(vc, animated: true)
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                } else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}
