//
//  Passcode.swift
//  Hi-TEK
//
//  Created by Admin on 30/03/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class Passcode: UIViewController {

    @IBOutlet var logoY: NSLayoutConstraint!
    @IBOutlet var btnForgotPin: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var btnSwipeToRecive: UIButton!
    @IBOutlet var viewDoted: UIView!
    
    var isFromLogin : Bool = false
    var isFromChangePin : Bool = false
    var stepNo : String = "1"
    var strPasscode1 : String = ""
    var strPasscode2 : String = ""
    var arrPasscode : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.view.frame.size.height <= 568 {
            logoY.constant = 40
        }
        
        btnForgotPin.isHidden = true
        btnSideMenu.isHidden = true
        if strSecretPin == "" {
            self.isFromLogin = true
        }
        btnSwipeToRecive.isHidden = true
        if UserDefaults.standard.value(forKey: IS_LOGIN) as! String == "1" && strSecretPin != "" {
            lblTitle.text = "Enter PIN"
            btnSwipeToRecive.isHidden = false
        }
        
        if isFromChangePin {
            lblTitle.text = "Enter PIN"
            btnSwipeToRecive.isHidden = true
            btnSideMenu.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        setDefault()
    }
    
    //Mark :- All button actions
    
    @IBAction func btnForgotPin(_ sender: Any) {
    }
    
    @IBAction func btnSwipeToRecive(_ sender: Any) {
        //let vc : MyWalletAddress = self.storyboard?.instantiateViewController(withIdentifier: "MyWalletAddress") as! MyWalletAddress
        //self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNumber(_ sender: UIButton) {
        if sender.tag == 10 {
            arrPasscode.add(0)
        } else if sender.tag == 11 {
            if arrPasscode.count > 0 {
                arrPasscode.removeLastObject()
            }
        } else {
            arrPasscode.add(sender.tag)
        }
        setActiveImage()
    }
    
    func setActiveImage() {
        for i in (1..<5) {
            let imgCode = self.view.viewWithTag(i*10) as! UIImageView
            if i > arrPasscode.count {
                imgCode.image = UIImage(named:"ic_dot")
            } else {
                imgCode.image = UIImage(named:"ic_dot_blue")
            }
        }
        
        if arrPasscode.count >= 4 {
            let when = DispatchTime.now() + 0.2
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.handlePin()
            }
        }
    }
    
    @IBAction func btnSideMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }
    
    //MARK:- Handle Pin
    func handlePin()
    {
        if isFromChangePin {
            if stepNo == "1" {
                let strPasscode : String = self.arrPasscode.componentsJoined(by: "")
                if strSecretPin == strPasscode {
                    stepNo = "2"
                    lblTitle.text = "Create Pin"
                    setDefault()
                } else {
                    self.view.makeToast("Invalid pin. Please try again.")
                    animationView(sender: viewDoted)
                    setDefault()
                }
            } else if stepNo == "2" {
                if strPasscode1 == "" {
                    strPasscode1 = self.arrPasscode.componentsJoined(by: "")
                    if strSecretPin == strPasscode1{
                        strPasscode1 = ""
                        self.view.makeToast("You can't use your current PIN as your new PIN")
                        animationView(sender: viewDoted)
                    } else {
                        lblTitle.text = "Confirm Pin"
                    }
                    setDefault()
                } else {
                    strPasscode2 = self.arrPasscode.componentsJoined(by: "")
                    if strPasscode1 == strPasscode2 {
                        gotoNextScreen(strPasscode: strPasscode1)
                    } else {
                        self.view.makeToast("PINs do not match")
                        animationView(sender: viewDoted)
                        setDefault()
                    }
                }
            }
        }
        else {
            if UserDefaults.standard.value(forKey: IS_LOGIN) as! String == "1" && strSecretPin != "" {
                let strPasscode : String = self.arrPasscode.componentsJoined(by: "")
                if strSecretPin == strPasscode {
                    gotoNextScreen(strPasscode: strPasscode)
                } else {
                    self.view.makeToast("Invalid pin. Please try again.")
                    animationView(sender: viewDoted)
                    setDefault()
                }
            } else {
                if strPasscode1 == "" {
                    strPasscode1 = self.arrPasscode.componentsJoined(by: "")
                    lblTitle.text = "Confirm Pin"
                    setDefault()
                } else {
                    strPasscode2 = self.arrPasscode.componentsJoined(by: "")
                    if strPasscode1 == strPasscode2 {
                        gotoNextScreen(strPasscode: strPasscode1)
                    } else {
                        self.view.makeToast("PINs do not match")
                        animationView(sender: viewDoted)
                        setDefault()
                    }
                }
            }
        }
    }
    
    func gotoNextScreen(strPasscode : String) {
        strSecretPin = strPasscode
        storeInUserDefault(strSecretPin, forKey: SECRETPIN)
        if isFromChangePin {
            NotificationCenter.default.post(name: Notification.Name("updateSelectedIndex"), object: nil)
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idDashboardVC)
            self.revealViewController().pushFrontViewController(vc, animated: true)
        }
        else {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSWRevealViewController)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setDefault() {
        arrPasscode = NSMutableArray.init()
        setActiveImage()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
