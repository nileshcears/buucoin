//
//  Verify.swift
//  Hi-TEK
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

protocol verifyDelegate {
    func closeVerifyPopUp(dictResponse : NSDictionary)
    func closePopUp()
}

class Verify: UIViewController {

    @IBOutlet var txtVerificationCode: UITextField!
    @IBOutlet var viewGradiant: UIView!
    
    var verifyDelegate : verifyDelegate?
    var isFromSendHiTEK : Bool = false
    var dictData : NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = viewGradiant.frame.size
        gradientLayer.colors =
            [Color_Hex(hex: "#0d75fc").cgColor, Color_Hex(hex: "#0fc9ec").cgColor]
        viewGradiant.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @IBAction func btnVerify(_ sender: Any) {
        if validateTxtFieldLength(txtVerificationCode, withMessage: EnterVerificationCode) &&
            validateEqaulTxtFieldLength(txtVerificationCode, withMessage: EnterValidVeriCode) {
            self.view.endEditing(true)
            if isFromSendHiTEK {
                let dictTmp : NSMutableDictionary = self.dictData.mutableCopy() as! NSMutableDictionary
                dictTmp.setObject(self.txtVerificationCode.text!, forKey: "verifycode" as NSCopying)
                self.verifyDelegate?.closeVerifyPopUp(dictResponse: dictTmp.copy() as! NSDictionary)
            } else {
                sendVerifyCodeReq()
            }
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.verifyDelegate?.closePopUp()
    }
    
    //MARK:- All API
    func sendVerifyCodeReq()
    {
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let dictParam : NSDictionary = ["twostepcode" : txtVerificationCode.text!]
            let strURL = String(format:"%@/verifycode",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                
                if dictResponse["success"] as? String == "1" {
                    self.verifyDelegate?.closeVerifyPopUp(dictResponse: dictResponse)
                }
                else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
