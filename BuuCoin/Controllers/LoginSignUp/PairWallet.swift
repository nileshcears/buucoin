//
//  PairWallet.swift
//  Hi-TEK
//
//  Created by Admin on 05/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PairWallet: UIViewController,QRCodeReaderViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    //MARK: - QrCode
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObject.ObjectType.qr], captureDevicePosition: .back)
            $0.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- UITableView Delegate Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RegisterCell") as! RegisterCell
        cell.btnScanPairingCode.addTarget(self, action: #selector(btnScanPairingCode), for: .touchUpInside)
        cell.btnCantAccessCam.addTarget(self, action: #selector(btnCantAccessCam), for: .touchUpInside)
        return cell
    }

    @objc func btnScanPairingCode() {
        scanInModalAction()
    }
    
    @objc func btnCantAccessCam() {
        let vc = loadVC(strStoryboardId: SB_LS, strVCId: idLoginVC) as! LoginVC
        vc.isFromPairWallet = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Call Api
    func sendPairhitekloginReq(strbuukey : String, qrtoken : String)
    {
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let dictParam : NSDictionary = NSDictionary() //["buukey" : strbuukey]
            let strURL = String(format:"%@/pairbuulogin",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(qrtoken, forHTTPHeaderField: "qrtoken")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                
                if dictResponse["success"] as? String == "1" {
                    if (dictResponse.object(forKey: "token") != nil) {
                        let token = (dictResponse.object(forKey: "token") as! String).aesDecrypt()
                        strToken = token.aesEncrypt()
                        storeInUserDefault(strToken, forKey: LOGIN_TOKEN)
                    }
                    
                    let arrData : NSArray = dictResponse.object(forKey: "data") as! NSArray
                    if arrData.count > 0 {
                        let dictData = dictionaryOfFilteredBy(dict: arrData.object(at: 0) as! NSDictionary)
                        storeInUserDefault(dictData, forKey: LOGIN_USER_DATA)
                        dictUserData = dictData
                    }
                    storeInUserDefault("1", forKey: IS_LOGIN)
                    
                    let vc = loadVC(strStoryboardId: SB_LS, strVCId: idPasscode) as! Passcode
                    vc.isFromLogin = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
    
    //MARK: - QRCode Scanner
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController?
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert?.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert?.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            case -11814:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert?.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            default:
                alert = nil
            }
            
            guard let vc = alert else { return false }
            
            present(vc, animated: true, completion: nil)
            
            return false
        }
    }
    func scanInModalAction()
    {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
                print(result.value)
                let strAddress : String = result.value
                let arrAddress = strAddress.components(separatedBy: "|")
                
                if arrAddress.count >= 2 {
                    let strbuukey: String = arrAddress[0]
                    let token: String = arrAddress[1].aesDecrypt()
                    let qrtoken: String = token.aesEncrypt()
                    self.sendPairhitekloginReq(strbuukey: strbuukey, qrtoken: qrtoken)
                }
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) {
            //self.sendValidateAddressReq(strAddress: result.value)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
