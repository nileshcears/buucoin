//
//  SplashVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 27/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet var scr1: UIScrollView!
    @IBOutlet var scr2: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.animate(withDuration: 5, delay: 0, options: ([.curveLinear, .curveEaseInOut]), animations: {() -> Void in
            self.scr1.setContentOffset(CGPoint(x: -633, y: 0), animated: true)
            self.scr2.setContentOffset(CGPoint(x: 633, y: 0), animated: true)
        }, completion: { (finished) in
            let transition = CATransition()
            transition.duration = 0.25
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionFade
            self.navigationController!.view.layer.add(transition, forKey: nil)
            if UserDefaults.standard.value(forKey: IS_LOGIN) as! String == "1" {
                let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSWRevealViewController)
                self.navigationController?.pushViewController(vc, animated: false)
            } else {
                let vc = loadVC(strStoryboardId: SB_LS, strVCId: idLoginVC)
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
            
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
