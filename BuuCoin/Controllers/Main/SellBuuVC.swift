//
//  SellBuuVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class SellBuuVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var txtCurrency: UITextField!
    @IBOutlet var txtBuuToCur: UITextField!
    @IBOutlet var lblBuuToCur: UILabel!
    @IBOutlet var lblSellCommission: UILabel!
    
    var SellCommission: Double = 0.0
    
    var strUSDRate: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtAmount.delegate = self
        if (UserDefaults.standard.object(forKey: "balancedetail") != nil) {
            let dictBalance = UserDefaults.standard.object(forKey: "balancedetail") as! NSDictionary
            strUSDRate = String(format : "%f", Double(dictBalance.object(forKey: "usd_rate") as? String ?? "0")!)
            lblBuuToCur.text = String(format : "1 Buu = %@ USD", strUSDRate)
            SellCommission = Double(dictBalance.object(forKey: "sold_commission") as? String ?? "0")!
            lblSellCommission.text = String(format : "%f%%", SellCommission)
        }
        txtAmount.addTarget(self, action: #selector(SellBuuVC.setRateInUSD(_:)), for: UIControlEvents.editingChanged)
    }

    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnSell(_ sender: Any) {
        if validateAmount(txtAmount, withMessage: EnterAmount) {
            sendSellBUUReq()
        }
    }

    //MARK:- UITextField Delegate Method
    @objc func setRateInUSD(_ textField : UITextField) {
        txtAmount.text = TRIM(string: txtAmount.text!)
        if txtAmount.text!.count <= 0 {
            txtBuuToCur.text = ""
        } else {
            var finalUSDRate: Double = Double(txtAmount.text ?? "0")! * Double(strUSDRate)!
            finalUSDRate = finalUSDRate - ((finalUSDRate * SellCommission) / 100)
            txtBuuToCur.text = String(format : "%f", finalUSDRate)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let dotsCount = textField.text!.components(separatedBy: ".").count - 1
        if dotsCount > 0 && (string == "." || string == ",") {
            return false
        }
        if string == "," {
            textField.text! += "."
            return false
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SellBuuVC {
    func sendSellBUUReq() {
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let strURL = String(format:"%@/sellbuu",BASE_URL)
            let dictParam: NSDictionary = ["buu_from" : txtAmount.text!,
                                           "usd_to" : txtBuuToCur.text!]
            print(dictParam)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                showMessage(dictResponse.object(forKey: "message") as! String)
                if dictResponse["success"] as? String == "1" {
                    self.txtAmount.text = ""
                    self.txtBuuToCur.text = ""
                }
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}
