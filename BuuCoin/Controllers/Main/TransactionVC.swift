//
//  TransactionVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 23/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    @IBOutlet var imgStatus: UIImageView!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblAmount: UILabel!
}

class TransactionVC: UIViewController {

    @IBOutlet var tblTransaction: UITableView!
    @IBOutlet var viewNDF: UIView!
    
    var arrTransactions : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNDF.alpha = 0
        sendTransactionReq()
    }

    //MARK:- All button actions
    @IBAction func btnMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TransactionVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TransactionCell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
        let dictTransaction : NSDictionary = arrTransactions.object(at: indexPath.row) as! NSDictionary
        let amount : Double = Double(dictTransaction.object(forKey: "amount") as! String)!
        let strStatus : String = dictTransaction.object(forKey: "status") as! String
        if strStatus == "Receive" {
            cell.lblAmount.text = String(format : "+%.8f",amount)
            cell.lblStatus.text = "Received"
            cell.imgStatus.image = UIImage(named: "ic_received")
        } else if strStatus == "Send" {
            cell.lblAmount.text = String(format : "%.8f",amount)
            cell.lblStatus.text = "Sent"
            cell.imgStatus.image = UIImage(named: "ic_sent")
        } else {
            cell.lblStatus.text = "Pending"
            cell.imgStatus.image = UIImage(named: "ic_pending_sent")
        }
 
        let date = NSDate(timeIntervalSince1970: Double(dictTransaction.object(forKey: "timestamp") as! String)!)
        cell.lblTime.text = timeAgoSince(date as Date)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idTransactionDetailsVC) as! TransactionDetailsVC
        vc.dictTransaction = arrTransactions.object(at: indexPath.row) as! NSDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
}

extension TransactionVC {
    func sendTransactionReq(){
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let strURL = String(format:"%@/gettrasaction",BASE_URL)
            let dictParam : NSDictionary = NSDictionary() //["buukey" : dictUserData.object(forKey: "buukey")!, "secretkey" : dictUserData.object(forKey: "secretkey")!]
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if dictResponse["success"] as? String == "1" {
                    let arrTmp : NSMutableArray = (arrayOfFilteredBy(arr: dictResponse.object(forKey: "data") as! NSArray)).mutableCopy() as! NSMutableArray
                    self.arrTransactions = NSMutableArray.init(array: arrTmp.sorted(by: { (Int(Double(($0 as! NSDictionary).value(forKey: "timestamp") as! String)!)) > (Int(Double((($1 as! NSDictionary).value(forKey: "timestamp") as! String))!)) }) )
                    if self.arrTransactions.count <= 0 {
                        self.viewNDF.alpha = 1
                    }
                    self.tblTransaction.reloadData()
                } else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}
