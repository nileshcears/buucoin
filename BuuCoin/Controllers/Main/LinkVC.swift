//
//  LinkVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 29/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit
import WebKit

protocol delegatePlaid {
    func getTokenID(token: String, accountID: String)
}

class LinkVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet var webView: WKWebView!
    
    var delegatePlaid: delegatePlaid?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let linkUrl = generateLinkInitializationURL()
        let url = URL(string: linkUrl)
        let request = URLRequest(url: url!)
        
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = false
        webView.scrollView.bounces = false
        webView.load(request)
    }
    
    // getUrlParams :: parse query parameters into a Dictionary
    func getUrlParams(url: URL) -> Dictionary<String, String> {
        var paramsDictionary = [String: String]()
        let queryItems = URLComponents(string: (url.absoluteString))?.queryItems
        queryItems?.forEach { paramsDictionary[$0.name] = $0.value }
        return paramsDictionary
    }
    
    // generateLinkInitializationURL :: create the link.html url with query parameters
    func generateLinkInitializationURL() -> String {
        let config = [
            "key": "378a7df673db51dd76b8f3e18b9d35",
            "env": "sandbox",
            "apiVersion": "v2", // set this to "v1" if using the legacy Plaid API
            "product": "auth",
            "selectAccount": "true",
            "clientName": "Test App",
            "isMobile": "true",
            "isWebview": "true",
            "webhook": "https://requestb.in",
            ]

        var components = URLComponents()
        components.scheme = "https"
        components.host = "cdn.plaid.com"
        components.path = "/link/v2/stable/link.html"
        components.queryItems = config.map { URLQueryItem(name: $0, value: $1) }
        return components.string!
    }
    
    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        
        let linkScheme = "plaidlink";
        let actionScheme = navigationAction.request.url?.scheme;
        let actionType = navigationAction.request.url?.host;
        let queryParams = getUrlParams(url: navigationAction.request.url!)
        
        if (actionScheme == linkScheme) {
            switch actionType {
                
            case "connected"?:
                print(queryParams)
                // Parse data passed from Link into a dictionary
                // This includes the public_token as well as account and institution metadata
                print("Public Token: \(queryParams["public_token"] ?? "")");
                print("Account ID: \(queryParams["account_id"] ?? "")");
                print("Institution type: \(queryParams["institution_type"] ?? "")");
                print("Institution name: \(queryParams["institution_name"] ?? "")");
                self.delegatePlaid?.getTokenID(token: queryParams["public_token"] ?? "", accountID: queryParams["account_id"] ?? "")
                _ = self.navigationController?.popViewController(animated: true)
                break
                
            case "exit"?:
                // Close the webview
                _ = self.navigationController?.popViewController(animated: true)
                
                // Parse data passed from Link into a dictionary
                // This includes information about where the user was in the Link flow
                // any errors that occurred, and request IDs
                print("URL: \(navigationAction.request.url?.absoluteString ?? "")")
                // Output data from Link
                print("User status in flow: \(queryParams["status"] ?? "")");
                // The requet ID keys may or may not exist depending on when the user exited
                // the Link flow.
                print("Link request ID: \(queryParams["link_request_id"] ?? "")");
                print("Plaid API request ID: \(queryParams["link_request_id"] ?? "")");
                break
                
            default:
                print("Link action detected: \(actionType ?? "")")
                break
            }
            
            decisionHandler(.cancel)
        } else if (navigationAction.navigationType == WKNavigationType.linkActivated &&
            (actionScheme == "http" || actionScheme == "https")) {
            // Handle http:// and https:// links inside of Plaid Link,
            // and open them in a new Safari page. This is necessary for links
            // such as "forgot-password" and "locked-account"
            UIApplication.shared.open(navigationAction.request.url!, options: [:], completionHandler: nil)
            decisionHandler(.cancel)
        } else {
            print("Unrecognized URL scheme detected that is neither HTTP, HTTPS, or related to Plaid Link: \(navigationAction.request.url?.absoluteString ?? "")");
            decisionHandler(.allow)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
