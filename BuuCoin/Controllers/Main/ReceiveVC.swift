//
//  ReceiveVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 23/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class ReceiveVC: UIViewController {

    @IBOutlet var qrY: NSLayoutConstraint!
    @IBOutlet var lblQRcodeAddress: UILabel!
    @IBOutlet var imgQRcode: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.view.frame.size.height <= 568 {
            qrY.constant = 10
        }
        lblQRcodeAddress.text = dictUserData.object(forKey: "walletaddress") as? String
        imgQRcode.image = convertTextToQRCode(text: lblQRcodeAddress.text!)
    }
    
    @IBAction func btnCopy(_ sender: Any) {
        let copyString : String = lblQRcodeAddress.text!
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = copyString
        self.view.makeToast("Address has been copied to the clipboard")
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let messageStr = lblQRcodeAddress.text!
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: [messageStr], applicationActivities: nil)
        activityViewController.excludedActivityTypes = nil
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
