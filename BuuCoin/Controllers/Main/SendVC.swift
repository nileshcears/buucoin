//
//  SendVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 23/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class SendVC: UIViewController, QRCodeReaderViewControllerDelegate {

    @IBOutlet var txtFrom: UITextField!
    @IBOutlet var txtTo: UITextField!
    @IBOutlet var txtAmount: UITextField!
    @IBOutlet var txtLabel: UITextField!
    @IBOutlet var txtMessage: UITextField!
    @IBOutlet var lblTransactionFees: UILabel!
    
    //MARK: - QrCode
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObject.ObjectType.qr], captureDevicePosition: .back)
            $0.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.object(forKey: "balancedetail") != nil) {
            let dictBalance = UserDefaults.standard.object(forKey: "balancedetail") as! NSDictionary
            lblTransactionFees.text = String(format: "Transaction fees : %@ Buu",dictBalance.object(forKey: "transaction_fee") as! String)
        }
        
    }

    @IBAction func btnQRCode(_ sender: Any) {
        self.view.endEditing(true)
        scanInModalAction()
    }
    
    @IBAction func btnSendBuu(_ sender: Any) {
        //txtTo.text = "LiDyTrWpBMT9JnogCraS8DzEKQAuRQ5uPo"
        if validateTxtFieldLength(txtTo, withMessage: EnterAddress) &&
            validateTxtFieldLength(txtAmount, withMessage: EnterAmount) &&
            validateAmount(txtAmount, withMessage: EnterAmount) {
            let dictParam : NSDictionary = ["send_to" : txtTo.text!,
                                            "send_amount" : txtAmount.text!,
                                            "verifycode" : ""]
            
            self.sendValidateAddressReq(strAddress: txtTo.text!,dictSendParam:dictParam)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SendVC {
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController?
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert?.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert?.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            case -11814:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert?.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            default:
                alert = nil
            }
            
            guard let vc = alert else { return false }
            
            present(vc, animated: true, completion: nil)
            
            return false
        }
    }
    func scanInModalAction()
    {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
                self.txtTo.text = result.value
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) {
            self.txtTo.text = result.value
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
}

extension SendVC {
    func sendValidateAddressReq(strAddress : String, dictSendParam : NSDictionary)
    {
        self.view.endEditing(true)
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let dictParam : NSDictionary = ["address" : strAddress]
            let strURL = String(format:"%@/validateaddress",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                
                if dictResponse["success"] as? String == "1" {
                    self.sendSendBuuReq(dictParam: dictSendParam)
                } else {
                    self.txtTo.text = ""
                    showMessage(dictResponse.object(forKey: "message") as! String)
                    hideLoaderHUD()
                }
                
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
    
    func sendSendBuuReq(dictParam : NSDictionary){
        self.view.endEditing(true)
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let strURL = String(format:"%@/sendbuucoin",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if dictResponse["success"] as? String == "1" {
                    self.txtTo.text = ""
                    self.txtAmount.text = ""
                    self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
                } else if dictResponse["success"] as? String == "2"{
                    let vc : Verify = Verify(nibName: "Verify", bundle: nil)
                    vc.verifyDelegate = self
                    vc.isFromSendHiTEK = true
                    vc.dictData = dictParam
                    self.presentPopupViewController(vc, animationType: MJPopupViewAnimationSlideBottomTop)
                } else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                hideLoaderHUD()
                
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}

extension SendVC: verifyDelegate {
    func closeVerifyPopUp(dictResponse : NSDictionary) {
        sendSendBuuReq(dictParam: dictResponse)
    }
    
    func closePopUp() {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationSlideTopBottom)
    }
}
