//
//  TransactionDetailsVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 23/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class TransactionDetailsVC: UIViewController {

    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblTo: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var imgTransactionType: UIImageView!
    
    var dictTransaction: NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dictTransaction)
        let amount : Double = Double(dictTransaction.object(forKey: "amount") as! String)!
        
        lblTo.text = dictTransaction.object(forKey: "to") as? String
        
        let strStatus : String = dictTransaction.object(forKey: "status") as! String

        if strStatus == "Receive" {
            lblAmount.text = String(format : "+%.8f",amount)
            lblTo.text = "My Buu Wallet"
            imgTransactionType.image = UIImage(named: "ic_received")
        } else if strStatus == "Send" {
            lblAmount.text = String(format : "%.8f",amount)
            imgTransactionType.image = UIImage(named: "ic_sent")
        } else if strStatus == "Pending" {
            lblAmount.text = String(format : "%.8f",amount)
            lblStatus.text = "Pending"
            imgTransactionType.image = UIImage(named: "ic_pending_sent")
        }

        let date = NSDate(timeIntervalSince1970: Double(dictTransaction.object(forKey: "timestamp") as! String)!)
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy hh:mm a"
        lblDate.text = formatter.string(from: date as Date)
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnExplore(_ sender: Any) {
        let strURL : String = String(format : "http://167.99.12.34/tx/%@",dictTransaction.object(forKey: "transactionHash") as! String)
        UIApplication.shared.open(URL(string: strURL)!, options: [:], completionHandler: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
