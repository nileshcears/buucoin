//
//  SideMenuVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 23/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    
    @IBOutlet var imgColorLine: UIImageView!
    @IBOutlet var btnFAAuth: UIButton!
}

class SideMenuVC: UIViewController {

    @IBOutlet var tblSideMenu: UITableView!
    var selectedRow : Int = 0
    var arrSideMenu : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        arrSideMenu = ["Dashboard","Send & Receive","Buy & Sell","Transactions","Chat","Change Pin","Email 2-step","Support","Logout"]
        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuVC.updateSelectedIndex), name: Notification.Name("updateSelectedIndex"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblSideMenu.isUserInteractionEnabled = true
        tblSideMenu.reloadData()
    }
    
    @objc func updateSelectedIndex(notification: Notification) {
        selectedRow = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SideMenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cell.lblTitle.text = arrSideMenu.object(at: indexPath.row) as? String
        cell.lblTitle.textColor = UIColor.white
        cell.imgColorLine.isHidden = true
        cell.btnFAAuth.isHidden = true
        if cell.lblTitle.text == "Email 2-step" {
            cell.btnFAAuth.isHidden = false
            cell.btnFAAuth.isSelected = true
            if str2FAauthActive == "0" {
                cell.btnFAAuth.isSelected = false
            }
            cell.btnFAAuth.addTarget(self, action: #selector(btnFAAuth(_:)), for: .touchUpInside)
        }
        if selectedRow == indexPath.row {
            cell.imgColorLine.isHidden = false
            cell.lblTitle.textColor = Color_Hex(hex: "1DAEF9")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblSideMenu.isUserInteractionEnabled = false
        let strMenu : String = arrSideMenu.object(at: indexPath.row) as! String
        if strMenu == "Dashboard" {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idDashboardVC)
            navigateToView(controller: vc)
        } else if strMenu == "Send & Receive" {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSendReceiveVC)
            navigateToView(controller: vc)
        } else if strMenu == "Buy & Sell" {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSendReceiveVC) as! SendReceiveVC
            vc.isExchange = true
            navigateToView(controller: vc)
        } else if strMenu == "Transactions" {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idTransactionVC)
            navigateToView(controller: vc)
        } else if strMenu == "Chat" {
            let vc = loadVC(strStoryboardId: SB_CHAT, strVCId: idConversationVC)
            navigateToView(controller: vc)
        } else if strMenu == "Change Pin"{
            let vc = loadVC(strStoryboardId: SB_LS, strVCId: idPasscode) as! Passcode
            vc.isFromChangePin = true
            navigateToView(controller: vc)
        } else if strMenu == "Support" {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idSupport)
            navigateToView(controller: vc)
        } else if strMenu == "Logout" {
            tblSideMenu.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "Are you sure you want to logout?", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes, I'm sure", style: .default, handler: { (alertAction) in
                logout()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            tblSideMenu.isUserInteractionEnabled = true
        }
        selectedRow = indexPath.row
        tblSideMenu.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    func navigateToView(controller : UIViewController)
    {
        let navigate : UINavigationController = UINavigationController.init(rootViewController: controller)
        navigate.navigationBar.isHidden = true
        self.revealViewController().pushFrontViewController(navigate, animated: true)
    }
    
    //MARK:- UITableView Button Action
    @objc func btnFAAuth(_ sender: UIButton)
    {
        var strMessage : String = "Do you want to disable 2-step authentication?"
        if !sender.isSelected {
            strMessage = "Protect your wallet from unauthorized access by enabling 2-step Verification. This code will be requested when you try to login in your wallet and when you try to send money. Do you want to enable 2-step authentication?"
        }
        let alertView = UIAlertController(title: "Email 2-step", message: strMessage, preferredStyle: .alert)
        let actionNo = UIAlertAction(title: "No", style: .cancel, handler: { (alert) in
            print("No")
        })
        let actionYes = UIAlertAction(title: "Yes", style: .default, handler: { (alert) in
            if sender.isSelected {
                self.sendUpdateStepReq(strTwoStep: "0")
            } else {
                self.sendUpdateStepReq(strTwoStep: "1")
            }
            self.tblSideMenu.reloadData()
        })
        
        alertView.addAction(actionNo)
        alertView.addAction(actionYes)
        self.present(alertView, animated: true, completion: nil)
    }
}

extension SideMenuVC {
    //MARK:- All API
    func sendUpdateStepReq(strTwoStep : String)
    {
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let dictParam : NSDictionary = ["twostep" : strTwoStep]
            let strURL = String(format:"%@/update2stap",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                
                if dictResponse["success"] as? String == "1" {
                    str2FAauthActive = strTwoStep
                    storeInUserDefault(str2FAauthActive, forKey: IS_2FAAUTH)
                } else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                self.tblSideMenu.reloadData()
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}
