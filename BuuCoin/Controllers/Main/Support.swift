//
//  Support.swift
//  Hi-TEK
//
//  Created by Admin on 04/04/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class Support: UIViewController {
    
    @IBOutlet var lblVersion: UILabel!
    @IBOutlet var lblSuppContact: UILabel!
    @IBOutlet var lblSuppSuggest: UILabel!
    @IBOutlet var lblSuppCareer: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            lblVersion.text = "Version \(version)"
        }
        
        if (UserDefaults.standard.object(forKey: "balancedetail") != nil) {
            let dictBalance = UserDefaults.standard.object(forKey: "balancedetail") as! NSDictionary
            lblSuppContact.text = dictBalance.object(forKey: "support_contact") as? String
            lblSuppSuggest.text = dictBalance.object(forKey: "support_suugest") as? String
            lblSuppCareer.text = dictBalance.object(forKey: "support_career") as? String
        }
    }
    
    @IBAction func btnMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
/*
{
    data =     {
        "btc_rate" = "0.0001";
        "buy_commission" = 8;
        confbalance = "3.03696496";
        "mail_body" = "Buucoin is the First cryptocurrency who provides Chat Pay Features in Mobile wallets.<br/><br/>Kindly download Mobile wallets from this link :<br/><br/>Android App: https://play.google.com/store/apps/details?id=com.buucoin<br/><br/>iOS App: https://app.itunes.com/buucoin<br/><br/>Now Enjoy Payment While chat with other Buu user.";
        "mail_subject" = "Invite to BuuCoin";
        mainbalance = "3.03696496";
        "sold_commission" = 3;
        "support_career" = "Make your career by joining in out development team on noreply@buucoins.io";
        "support_contact" = "Contact to our blockchain department on noreply@buucoins.io";
        "support_feedback" = "If you still have any question or comments or if you wish to connect us for any suggestions, simply mail us on noreply@buucoins.io";
        "support_suugest" = "Drop your valuable suggestions about our app via noreply@buucoins.io";
        "transaction_fee" = "0.001";
        unconfbalance = "0.29900000";
        "usd_rate" = "0.64683251204";
    };
    message = "Yahoo get balance success :)";
    success = 1;
}*/
