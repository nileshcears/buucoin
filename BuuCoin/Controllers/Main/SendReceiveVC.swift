//
//  SendReceiveVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 23/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class SendReceiveVC: UIViewController {

    var pageMenu : CAPSPageMenu?
    var isExchange: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPageMenu()
    }

    @IBAction func btnMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SendReceiveVC {
    func setPageMenu()
    {
        var arrController : [UIViewController] = []
        
        if isExchange {
            let vcBuy = loadVC(strStoryboardId: SB_MAIN, strVCId: idBuyBuuVC) as! BuyBuuVC
            vcBuy.title = "Buy Buu"
            arrController.append(vcBuy)
            
            let vcSell = loadVC(strStoryboardId: SB_MAIN, strVCId: idSellBuuVC) as! SellBuuVC
            vcSell.title = "Sell Buu"
            arrController.append(vcSell)
        } else {
            let vcSend = loadVC(strStoryboardId: SB_MAIN, strVCId: idSendVC) as! SendVC
            vcSend.title = "Send Buu"
            arrController.append(vcSend)
            
            let vcRecieve = loadVC(strStoryboardId: SB_MAIN, strVCId: idReceiveVC) as! ReceiveVC
            vcRecieve.title = "Recieve Buu"
            arrController.append(vcRecieve)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.clear),
            .viewBackgroundColor(UIColor.clear),
            .selectionIndicatorColor(Color_Hex(hex: "1DAEF9")),
            .bottomMenuHairlineColor(Color_Hex(hex: "9296B4")),
            .selectedMenuItemLabelColor(UIColor.white),
            .unselectedMenuItemLabelColor(Color_Hex(hex: "9296B4")),
            .selectionIndicatorHeight(2),
            .menuHeight(54),
            .menuMargin(0),
            .menuItemFont(UIFont(name: "Montserrat-Regular", size: 15)!),
            .menuItemWidth(self.view.frame.size.width/CGFloat(arrController.count)),
            .centerMenuItems(true)
        ]
        
        let frame = CGRect(x: 0, y: 74, width: self.view.frame.size.width, height: self.view.frame.size.height-74)
        
        pageMenu = CAPSPageMenu(viewControllers: arrController, frame: frame, pageMenuOptions: parameters)
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
    }
}
