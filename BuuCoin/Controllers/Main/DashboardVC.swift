//
//  DashboardVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 23/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class BalanceCell: UITableViewCell {
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var imgGraph: UIImageView!
    @IBOutlet var imgupDown: UIImageView!
}

class DashboardVC: UIViewController {

    @IBOutlet var lblBalance: UILabel!
    @IBOutlet var tblBalance: UITableView!
    @IBOutlet var btnRefresh: UIButton!
    
    var isStopAnim: Bool = false
    var dictBalance: NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        if (UserDefaults.standard.object(forKey: "balancedetail") != nil) {
            dictBalance = UserDefaults.standard.object(forKey: "balancedetail") as! NSDictionary
            lblBalance.text = String(format : "%.8f",Double(self.dictBalance.object(forKey: "mainbalance") as! String)!)
        }
        startAnimation()
        sendGetWalletBalanceReq()
    }
    
    //MARK:- All button actions
    @IBAction func btnSideMenu(_ sender: Any) {
        self.revealViewController().revealToggle(animated:true)
    }

    @IBAction func btnRefresh(_ sender: UIButton) {
        isStopAnim = false
        startAnimation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.sendGetWalletBalanceReq()
        }
    }
    
    func stopRefreshAnimation() {
        isStopAnim = true
        self.tblBalance.reloadData()
        self.btnRefresh.layer.removeAllAnimations()
    }
    
    func startAnimation() {
        if !isStopAnim {
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveLinear, animations: { () -> Void in
                self.btnRefresh.transform = self.btnRefresh.transform.rotated(by: .pi/4)
            }) { (finished) -> Void in
                self.startAnimation()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension DashboardVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : BalanceCell = tableView.dequeueReusableCell(withIdentifier: "BalanceCell", for: indexPath) as! BalanceCell
        cell.lblAmount.text = "0.00000000"
        if indexPath.row == 0 {
            cell.lblStatus.text = "Confirm Balance"
            if let strConfBalance = dictBalance.object(forKey: "confbalance") as? String {
                cell.lblAmount.text = strConfBalance
            }
        } else if indexPath.row == 1 {
            cell.lblStatus.text = "Unconfirm Balance"
            if let strUnConfBalance = dictBalance.object(forKey: "unconfbalance") as? String {
                cell.lblAmount.text = strUnConfBalance
            }
        } else if indexPath.row == 2 {
            cell.lblStatus.text = "Rate of Buu in BTC"
            if let strBTCRate = dictBalance.object(forKey: "btc_rate") as? String {
                cell.lblAmount.text = String(format : "%@ BTC",strBTCRate)
            }
        } else {
            cell.lblStatus.text = "Rate of USD"
            if let strUSDRate = dictBalance.object(forKey: "usd_rate") as? String {
                cell.lblAmount.text = String(format : "%f", Double(strUSDRate)!)
            }
        }
        
        
        let myInt = arc4random_uniform(5) + 1
        if myInt % 2 == 0 {
            cell.imgupDown.image = UIImage(named: "ic_arrow_green_up")
        } else {
            cell.imgupDown.image = UIImage(named: "ic_arrow_red_down")
        }
        cell.imgGraph.image = UIImage(named: "graph_\(myInt)")

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
}

extension DashboardVC {
    func sendGetWalletBalanceReq() {
        if isConnectedToNetwork() {
            let strURL = String(format:"%@/getwalletbalance",BASE_URL)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: nil, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                if dictResponse["success"] as? String == "1" {
                    self.dictBalance = dictionaryOfFilteredBy(dict: dictResponse.object(forKey: "data") as! NSDictionary)
                    storeInUserDefault(self.dictBalance, forKey: "balancedetail")
                    self.lblBalance.text = String(format : "%.8f",Double(self.dictBalance.object(forKey: "mainbalance") as! String)!)
                } else {
                    showMessage(dictResponse.object(forKey: "message") as! String)
                }
                self.stopRefreshAnimation()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                self.stopRefreshAnimation()
            }
        } else {
            showMessage("No Internet Connection!")
            stopRefreshAnimation()
        }
    }
}
