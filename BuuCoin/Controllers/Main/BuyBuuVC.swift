//
//  BuyBuuVC.swift
//  BuuCoin
//
//  Created by iOS_1 on 24/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit

class BuyBuuVC: UIViewController, UITextFieldDelegate, delegatePlaid {
    func getTokenID(token: String, accountID: String) {
        sendBuyBUUReq(token: token, accountID: accountID)
    }   

    @IBOutlet var txtAmountUSD: UITextField!
    @IBOutlet var lblUsdToBuu: UILabel!
    @IBOutlet var txtBuuAmount: UITextField!
    @IBOutlet var lblBuyCommission: UILabel!
    
    var BuyCommission: Double = 0.0
    
    var strUSDRate: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtAmountUSD.delegate = self
        if (UserDefaults.standard.object(forKey: "balancedetail") != nil) {
            let dictBalance = UserDefaults.standard.object(forKey: "balancedetail") as! NSDictionary
            strUSDRate = String(format : "%f", Double(dictBalance.object(forKey: "usd_rate") as? String ?? "0")!)
            lblUsdToBuu.text = String(format : "1 USD = %f BUU", 1/Double(strUSDRate)!)
            BuyCommission = Double(dictBalance.object(forKey: "buy_commission") as? String ?? "0")!
            lblBuyCommission.text = String(format : "%f%%", BuyCommission)
        }
        txtAmountUSD.addTarget(self, action: #selector(BuyBuuVC.setRateInBUU(_:)), for: UIControlEvents.editingChanged)
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBuy(_ sender: Any) {
        if validateAmount(txtAmountUSD, withMessage: EnterAmount) {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: idLinkVC) as! LinkVC
            vc.delegatePlaid = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- UITextField Delegate Method
    @objc func setRateInBUU(_ textField : UITextField) {
        txtAmountUSD.text = TRIM(string: txtAmountUSD.text!)
        if txtAmountUSD.text!.count <= 0 {
            txtBuuAmount.text = ""
        } else {
            var finalUSDRate: Double = Double(txtAmountUSD.text ?? "0")! / Double(strUSDRate)!
            finalUSDRate = finalUSDRate - ((finalUSDRate * BuyCommission) / 100)
            txtBuuAmount.text = String(format : "%f", finalUSDRate)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let dotsCount = textField.text!.components(separatedBy: ".").count - 1
        if dotsCount > 0 && (string == "." || string == ",") {
            return false
        }
        if string == "," {
            textField.text! += "."
            return false
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension BuyBuuVC {
    func sendBuyBUUReq(token: String, accountID: String) {
        if isConnectedToNetwork() {
            showLoaderHUD(strMessage: "")
            let strURL = String(format:"%@/banktransfer",BASE_URL)
            let dictParam: NSDictionary = ["usd_from" : txtAmountUSD.text!,
                                           "buu_to" : txtBuuAmount.text!,
                                           "public_token":token,
                                           "account_id":accountID]
            print(dictParam)
            let manager = AFHTTPRequestOperationManager()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.setValue(strToken, forHTTPHeaderField: "Token")
            manager.post(strURL, parameters: dictParam, success: { (operation, responseObject) in
                print(responseObject ?? "Response got nil")
                let dictResponse : NSDictionary = dictionaryOfFilteredBy(dict: responseObject as! NSDictionary)
                showMessage(dictResponse.object(forKey: "message") as! String)
                if dictResponse["success"] as? String == "1" {
                    self.txtAmountUSD.text = ""
                    self.txtBuuAmount.text = ""
                }
                hideLoaderHUD()
            }) { (operation, error) in
                print(operation?.responseString ?? "responseString Not Found")
                if let dictData = operation?.responseObject as? NSDictionary {
                    authFail(dictData: dictData)
                }
                hideLoaderHUD()
            }
        } else {
            showMessage("No Internet Connection!")
        }
    }
}

//extension String {
//    func calculateUSDToBuu() -> String {
//        
//        
//        return
//        1 buu = 0.65 USD
//        ? = 1 USD
//    }
//}

