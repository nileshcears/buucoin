//
//  LoadingAnimationView.swift
//  LoadingView
//
//  Created by Admin on 15/03/18.
//  Copyright © 2018 el. All rights reserved.
//

import UIKit
public enum LoadingAnimationType {
    case lineScale
}

class AnimationFactory {
    class func animationForType(_ type: LoadingAnimationType) -> LoadingAnimationDelegate {
        switch type {
        case .lineScale:
            return LineScale()
        }
    }
}
protocol LoadingAnimationDelegate: class {
    func setup(_ layer: CALayer, size: CGSize, colors: [UIColor])
}
open class LoadingAnimationView: UIView
{
    fileprivate static let defaultType = LoadingAnimationType.lineScale
    fileprivate static let defaultColors = [UIColor.black]
    fileprivate static let defaultSize = CGSize(width: 40, height: 40)
    
    fileprivate var type: LoadingAnimationType
    fileprivate var colors: [UIColor]
    fileprivate var Size: CGSize
    
    required public init?(coder aDecoder: NSCoder)
    {
        self.type = LoadingAnimationView.defaultType
        self.colors = LoadingAnimationView.defaultColors
        self.Size = LoadingAnimationView.defaultSize
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, type: LoadingAnimationType = defaultType,color :[UIColor], size: CGSize = defaultSize)
    {
        self.type = type
        self.colors = color
        self.Size = size
        super.init(frame: frame)
    }
    
    func setupAnimation()
    {
        let animation = AnimationFactory.animationForType(type)
        layer.sublayers = nil
        animation.setup(layer, size: Size, colors: colors)
    }
}
