//
//  LoadingHud.swift
//  OneLifeToGo
//
//  Created by Admin on 16/03/18.
//  Copyright © 2018 el. All rights reserved.
//

import Foundation
import UIKit

class LoadingHud  : NSObject{
    
    static var bgview = UIView()
    static var anim1 = LoadingAnimationView.init(frame: CGRect.zero, color: [UIColor.clear])
    static var lbl  = UILabel()
    class func showHUDIn(view:UIView, withText text:String){
        //REMOVE IF ALREADY EXIST
        bgview.removeFromSuperview()

        let colorset : [UIColor] = loadercolor
        bgview.frame = CGRect.init(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        bgview.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.7)

        //REMOVE IF ALREADY ADDED
        anim1.removeFromSuperview()

        //LINE SCALE
        anim1 = LoadingAnimationView.init(frame: CGRect.init(x: view.frame.size.width/2 - 15, y: view.center.y - 50 , width: 5, height: 80), type: .lineScale,color: colorset, size: CGSize.init(width: 50, height: 90))
        anim1.setupAnimation()
        bgview.addSubview(anim1)
        //LABEL
        if text.count > 0
        {
            lbl.removeFromSuperview()
            lbl = UILabel.init(frame: CGRect.init(x: 0, y: view.center.y + 20, width: view.frame.size.width, height: 15))
            lbl.text = ""
            lbl.backgroundColor = UIColor.clear
            lbl.text = text.uppercased()
            lbl.font = UIFont.boldSystemFont(ofSize: 12)
            lbl.textColor = UIColor.white
            lbl.textAlignment = .center
            bgview.addSubview(lbl)
        }
        //SHOW IN VIEW
        view.addSubview(bgview)
    }
    
    class func showHUD(withText text:String){
        LoadingHud.showHUDIn(view: UIApplication.shared.windows[0], withText: text)
    }
    
    class func showDefaultHUD(){
        LoadingHud.showHUD(withText: "")
    }
    
    class func showHUD(){
        LoadingHud.showHUD(withText: "")
    }
    
    class func showHUDText(withText text:String){
        
         LoadingHud.showHUD(withText: text)
    }
    class func dismissHUD(){
       bgview.removeFromSuperview()
    }
    
    
}
