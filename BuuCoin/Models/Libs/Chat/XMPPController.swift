//
//  XMPPController.swift
//  CrazyMessages
//
//  Created by Andres on 7/21/16.
//  Copyright © 2016 Andres. All rights reserved.
//

import Foundation
import XMPPFramework
import SwiftyJSON
import MobileCoreServices
import AVFoundation
import AVKit


enum XMPPControllerError: Error {
	case wrongUserJID
}

class XMPPController: NSObject {
	var xmppStream: XMPPStream
    let xmppRosterStorage = XMPPRosterCoreDataStorage()
   
    
//    var xmppRoster: XMPPRoster
    
	let hostName: String
    var userJID: XMPPJID
	let hostPort: UInt16
	let password: String
	
    init(hostName: String, userJIDString: String, hostPort: UInt16 = 5222, password: String) throws {
        guard let userJID = XMPPJID(string: userJIDString) else {
			throw XMPPControllerError.wrongUserJID
		}
		self.hostName = hostName
		self.userJID = userJID
		self.hostPort = hostPort
		self.password = password
		// Stream Configuration
		self.xmppStream = XMPPStream()
		self.xmppStream.hostName = hostName
		self.xmppStream.hostPort = hostPort
		self.xmppStream.startTLSPolicy = XMPPStreamStartTLSPolicy.allowed
		self.xmppStream.myJID = userJID
        
        super.init()
		self.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
	}
    

    func connect() {
        if self.xmppStream.isDisconnected {
            try! self.xmppStream.connect(withTimeout: XMPPStreamTimeoutNone)
        }
	}
    
    func disconnect() {
        if self.xmppStream.isConnected {
            self.xmppStream.disconnect()
        }
    }
    
    
    func getOnlineUser(strUserJabbarID:String)
    {
            let query = try! DDXMLElement(xmlString: "<query xmlns='jabber:iq:roster' node='all users'/>")
            let userReceiverJID = XMPPJID(string: strUserJabbarID)
            let iq = XMPPIQ(type: "get", to: userReceiverJID, elementID: xmppStream.generateUUID, child: query)
            self.xmppStream.send(iq)
    }
    
    func  sendMessage(dicMessage:NSDictionary,strReceiverJabbarID:String)  {
        let userReceiverJID = XMPPJID(string: "\(strReceiverJabbarID)@\(CHATHOST)")
        let objMessage:XMPPMessage = XMPPMessage.init(type: "chat", to: userReceiverJID )
        convert(dicData: dicMessage) { (strMessage) in
            objMessage.addBody(strMessage)
            self.xmppStream.send(objMessage)
        }
    }
}

extension XMPPController: XMPPStreamDelegate {

    func xmppStreamDidConnect(_ stream: XMPPStream) {
		print("Stream: Connected")
		try! stream.authenticate(withPassword: self.password)
	}
	
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
		self.xmppStream.send(XMPPPresence())
		print("Stream: Authenticated")
	}
    
    func xmppStream(_ sender: XMPPStream, willReceive message: XMPPMessage) -> XMPPMessage? {
        //NEW MESSAGE MANAGE IN LOCAL DATABASE
        let newMessage  = "\(message.elements(forName: "body")[0].children![0])"
        convert(JSONstring: newMessage) { (dicMessage) in
            let jsonData = JSON(dicMessage)
            let objMessage:ChatMessage = ChatMessage.init(json: jsonData)
            switch objMessage.messagetype ?? 0
            {
            case 0:
                    DBManager.shared.insertNewTextMessage(dicMessage: dicMessage)
                    self.checkForRefresh(strReceiverID: "\(objMessage.receiverId!)")
                break
            case 1:
                DBManager.shared.insertNewTextMessage(dicMessage: dicMessage)
                self.checkForRefresh(strReceiverID: "\(objMessage.receiverId!)")
                break
            default:
                break

            }
        }
        return nil
    }
    
    
  
    
    func checkForRefresh(strReceiverID:String)
    {
        if(strReceiverID == APP_DELEGATE.currentChatUserID)
        {
            NotificationCenter.default.post(name: Notification.Name("ReceivedNewMessage"), object: nil)
        }
        
        if(APP_DELEGATE.isChatListScreen ==  true)
        {
            NotificationCenter.default.post(name: Notification.Name("UpdateChatList"), object: nil)
        }
    }

    func getTopViewController() -> UIViewController?
    {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
    
    func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        print("Did send message")
    }
	
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
		print("Stream: Fail to Authenticate")
	}
    

    func xmppStream(_ sender: XMPPStream, didReceive presence: XMPPPresence) {
        let presenceType = presence.type
        let status = presence.status
        let myUsername = sender.myJID?.user
        let presenceFromUser = presence.from?.user as! String
        
        if presenceFromUser != myUsername {
            print("Did receive presence from \(presenceFromUser)")
            print("Type \(presenceType)")
            print("Type \(status)")
            
//            if presenceType == "subscribe" {
//                delegate.buddyWentOnline(name: "\(presenceFromUser)@domainName")
//            } else if presenceType == "unavailable" {
//                delegate.buddyWentOffline(name: "\(presenceFromUser)@domainName")
//            }
        }
    }

}
