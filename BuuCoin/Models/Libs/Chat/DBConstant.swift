//
//  DBConstant.swift
//  DoctorApp
//
//  Created by Cears on 11/08/18.
//  Copyright © 2018 Cears. All rights reserved.
//

import Foundation

//DATEBASE NAME
let APP_DATABASE = "Buu.sqlite"
let CHATHOST = "142.93.242.24"


let STATUS_FAILED = -1
let STATUS_SENT = 0
let STATUS_DELIVERED = 1
let STATUS_READ = 2

let MSG_TYPE_TEXT = 0
let MSG_TYPE_IMAGE = 1



//TABLE NAME
let  tbl_Discussion = "Discussion"
let  tbl_ChatData = "ChatData"


//TABLE FIELD tbl_Discussion
let Chat_ID = "chatId"
let Chat_User_ID = "chat_user_id"
let Login_User_ID = "login_user_id"
let Profile_Pic = "profile_pic"
let Firstname = "firstname"
let Lastname = "lastname"
let Lastmessage = "lastmessage"
let Lastmessage_Time = "lastmessage_time"

//TABLE FIELD tbl_ChatData
let Message_ID = "iMessageId"
let Sender_Profiel_Pic = "vSenderProfile"
let Sender_Firstname = "vSenderName"
let Sender_ID = "iSenderId"
let Receiver_ID = "iReceiverId"
let Message_Type = "iMessageType"
let Message = "vMessage"
let Read_Status = "iReadStatus"
let Deleted = "iDeleted"
let Created_Date = "vCreatedDate"
let Latitude = "vLatitude"
let Longitude = "vLongitude"
let Amount = "iAmount"
let SenderAddress = "vSenderAddress"
let ReceiverAddress = "vReceiverAddress"

let Media = "media" //no need
let Contact_ID = "contact_id" //no need
let Contact_Name = "contact_name" //no need
let Contact_Pic = "contact_pic" //no need
let Contact_Number = "contact_number" //no need
let MeetingDetails = "meeting" //no need
let Sender_Lastname = "sender_lastname" //no need
let Receiver_Profiel_Pic = "receiver_profile_pic" //no need
let Receiver_Firstname = "receiver_firstname" //no need
let Receiver_Lastname = "receiver_lastname" //no need

//iChatId
//iRequestBy
//iRequestStatus
//vComment

/*{
    "iAmount": 0,
    "iChatId": 1,
    "iDeleted": 0,
    "iMessageId": "d1e3ef80-3e76-48c2-87bd-d73468eda9d6",
    "iMessageType": 0,
    "iReadStatus": 1,
    "iReceiverId": 8,
    "iRequestBy": 6,
    "iRequestStatus": 0,
    "iSenderId": 6,
    "vComment": "",
    "vCreatedDate": "2018-09-27 10:31:53",
    "vLatitude": "",
    "vLongitude": "",
    "vMessage": "hello",
    "vReceiverAddress": "bxdVpCkoUvA9j2MkNHCcWfCreKkq4pYe2KfqDY4RAP38U4Qcgu3wbwZVbpSSshaXWS2J9p7rXhdz4XRf1NoMgK2y1C2adQ22f",
    "vSenderAddress": "bxcrYgV4J57KVewvs4FrNm9UWKuyFoqUwcKJ4X5r8GTXB5qwDaNrdkFVbpSSshaXWS2J9p7rXhdz4XRf1NoMgK2y1C2X8Az5z",
    "vSenderName": "Android 1",
    "vSenderProfile": ""
}*/

/*
const val STATUS_FAILED = -1
const val STATUS_SENT = 0
const val STATUS_DELIVERED = 1
const val STATUS_READ = 2

const val MSG_TYPE_TEXT = 0
const val MSG_TYPE_PAY = 1
const val MSG_TYPE_REQUEST = 2

const val REQUEST_STATUS_PENDING = 0
const val REQUEST_STATUS_ACCEPT = 1
const val REQUEST_STATUS_DECLINE = 2 */


