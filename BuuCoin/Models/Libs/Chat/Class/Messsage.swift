//
//  Messsage.swift
//
//  Created by Cears on 06/09/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Messsage: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let stanza = "stanza"
    static let body = "body"
    static let conversationID = "conversationID"
    static let messageID = "messageID"
    static let toJID = "toJID"
    static let sentDate = "sentDate"
    static let fromJID = "fromJID"
    static let fromJIDResource = "fromJIDResource"
  }

  // MARK: Properties
  public var stanza: String?
  public var body: String?
  public var conversationID: Int?
  public var messageID: Int?
  public var toJID: String?
  public var sentDate: Int?
  public var fromJID: String?
  public var fromJIDResource: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    stanza = json[SerializationKeys.stanza].string
    body = json[SerializationKeys.body].string
    conversationID = json[SerializationKeys.conversationID].int
    messageID = json[SerializationKeys.messageID].int
    toJID = json[SerializationKeys.toJID].string
    sentDate = json[SerializationKeys.sentDate].int
    fromJID = json[SerializationKeys.fromJID].string
    fromJIDResource = json[SerializationKeys.fromJIDResource].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = stanza { dictionary[SerializationKeys.stanza] = value }
    if let value = body { dictionary[SerializationKeys.body] = value }
    if let value = conversationID { dictionary[SerializationKeys.conversationID] = value }
    if let value = messageID { dictionary[SerializationKeys.messageID] = value }
    if let value = toJID { dictionary[SerializationKeys.toJID] = value }
    if let value = sentDate { dictionary[SerializationKeys.sentDate] = value }
    if let value = fromJID { dictionary[SerializationKeys.fromJID] = value }
    if let value = fromJIDResource { dictionary[SerializationKeys.fromJIDResource] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.stanza = aDecoder.decodeObject(forKey: SerializationKeys.stanza) as? String
    self.body = aDecoder.decodeObject(forKey: SerializationKeys.body) as? String
    self.conversationID = aDecoder.decodeObject(forKey: SerializationKeys.conversationID) as? Int
    self.messageID = aDecoder.decodeObject(forKey: SerializationKeys.messageID) as? Int
    self.toJID = aDecoder.decodeObject(forKey: SerializationKeys.toJID) as? String
    self.sentDate = aDecoder.decodeObject(forKey: SerializationKeys.sentDate) as? Int
    self.fromJID = aDecoder.decodeObject(forKey: SerializationKeys.fromJID) as? String
    self.fromJIDResource = aDecoder.decodeObject(forKey: SerializationKeys.fromJIDResource) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(stanza, forKey: SerializationKeys.stanza)
    aCoder.encode(body, forKey: SerializationKeys.body)
    aCoder.encode(conversationID, forKey: SerializationKeys.conversationID)
    aCoder.encode(messageID, forKey: SerializationKeys.messageID)
    aCoder.encode(toJID, forKey: SerializationKeys.toJID)
    aCoder.encode(sentDate, forKey: SerializationKeys.sentDate)
    aCoder.encode(fromJID, forKey: SerializationKeys.fromJID)
    aCoder.encode(fromJIDResource, forKey: SerializationKeys.fromJIDResource)
  }

}
