//
//  Meeting.swift
//
//  Created by Cears on 05/09/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Meeting: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let meetingTimeTo = "meeting_time_to"
    static let meetingSubject = "meeting_subject"
    static let iId = "iId"
    static let meetingId = "meeting_id"
    static let meetingTimeFrom = "meeting_time_from"
    static let meetingStatus = "meeting_status"
    static let meetingDate = "meeting_date"
    static let meetingMessage = "meeting_message"
    static let meetingOwnerId = "meeting_owner_id"
    static let meetingType = "meeting_type"
  }

  // MARK: Properties
  public var meetingTimeTo: String?
  public var meetingSubject: String?
  public var iId: Int?
  public var meetingId: Int?
  public var meetingTimeFrom: String?
  public var meetingStatus: Int?
  public var meetingDate: String?
  public var meetingMessage: String?
  public var meetingOwnerId: Int?
  public var meetingType: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    meetingTimeTo = json[SerializationKeys.meetingTimeTo].string
    meetingSubject = json[SerializationKeys.meetingSubject].string
    iId = json[SerializationKeys.iId].int
    meetingId = json[SerializationKeys.meetingId].int
    meetingTimeFrom = json[SerializationKeys.meetingTimeFrom].string
    meetingStatus = json[SerializationKeys.meetingStatus].int
    meetingDate = json[SerializationKeys.meetingDate].string
    meetingMessage = json[SerializationKeys.meetingMessage].string
    meetingOwnerId = json[SerializationKeys.meetingOwnerId].int
    meetingType = json[SerializationKeys.meetingType].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = meetingTimeTo { dictionary[SerializationKeys.meetingTimeTo] = value }
    if let value = meetingSubject { dictionary[SerializationKeys.meetingSubject] = value }
    if let value = iId { dictionary[SerializationKeys.iId] = value }
    if let value = meetingId { dictionary[SerializationKeys.meetingId] = value }
    if let value = meetingTimeFrom { dictionary[SerializationKeys.meetingTimeFrom] = value }
    if let value = meetingStatus { dictionary[SerializationKeys.meetingStatus] = value }
    if let value = meetingDate { dictionary[SerializationKeys.meetingDate] = value }
    if let value = meetingMessage { dictionary[SerializationKeys.meetingMessage] = value }
    if let value = meetingOwnerId { dictionary[SerializationKeys.meetingOwnerId] = value }
    if let value = meetingType { dictionary[SerializationKeys.meetingType] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.meetingTimeTo = aDecoder.decodeObject(forKey: SerializationKeys.meetingTimeTo) as? String
    self.meetingSubject = aDecoder.decodeObject(forKey: SerializationKeys.meetingSubject) as? String
    self.iId = aDecoder.decodeObject(forKey: SerializationKeys.iId) as? Int
    self.meetingId = aDecoder.decodeObject(forKey: SerializationKeys.meetingId) as? Int
    self.meetingTimeFrom = aDecoder.decodeObject(forKey: SerializationKeys.meetingTimeFrom) as? String
    self.meetingStatus = aDecoder.decodeObject(forKey: SerializationKeys.meetingStatus) as? Int
    self.meetingDate = aDecoder.decodeObject(forKey: SerializationKeys.meetingDate) as? String
    self.meetingMessage = aDecoder.decodeObject(forKey: SerializationKeys.meetingMessage) as? String
    self.meetingOwnerId = aDecoder.decodeObject(forKey: SerializationKeys.meetingOwnerId) as? Int
    self.meetingType = aDecoder.decodeObject(forKey: SerializationKeys.meetingType) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(meetingTimeTo, forKey: SerializationKeys.meetingTimeTo)
    aCoder.encode(meetingSubject, forKey: SerializationKeys.meetingSubject)
    aCoder.encode(iId, forKey: SerializationKeys.iId)
    aCoder.encode(meetingId, forKey: SerializationKeys.meetingId)
    aCoder.encode(meetingTimeFrom, forKey: SerializationKeys.meetingTimeFrom)
    aCoder.encode(meetingStatus, forKey: SerializationKeys.meetingStatus)
    aCoder.encode(meetingDate, forKey: SerializationKeys.meetingDate)
    aCoder.encode(meetingMessage, forKey: SerializationKeys.meetingMessage)
    aCoder.encode(meetingOwnerId, forKey: SerializationKeys.meetingOwnerId)
    aCoder.encode(meetingType, forKey: SerializationKeys.meetingType)
  }

}
