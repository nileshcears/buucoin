//
//  ChatList.swift
//
//  Created by Cears on 06/09/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ChatList: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let messsage = "messsage"
    static let lastName = "last_name"
    static let id = "id"
    static let firstName = "first_name"
    static let profile = "profile"
  }

  // MARK: Properties
  public var messsage: Messsage?
  public var lastName: String?
  public var id: Int?
  public var firstName: String?
  public var profile: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    messsage = Messsage(json: json[SerializationKeys.messsage])
    lastName = json[SerializationKeys.lastName].string
    id = json[SerializationKeys.id].int
    firstName = json[SerializationKeys.firstName].string
    profile = json[SerializationKeys.profile].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = messsage { dictionary[SerializationKeys.messsage] = value.dictionaryRepresentation() }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    if let value = profile { dictionary[SerializationKeys.profile] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.messsage = aDecoder.decodeObject(forKey: SerializationKeys.messsage) as? Messsage
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.profile = aDecoder.decodeObject(forKey: SerializationKeys.profile) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(messsage, forKey: SerializationKeys.messsage)
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(profile, forKey: SerializationKeys.profile)
  }

}
