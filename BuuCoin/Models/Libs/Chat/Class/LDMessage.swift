//
//  LDMessage.swift
//
//  Created by Cears on 13/09/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class LDMessage: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let media = "media"
    static let contactNumber = "contact_number"
    static let latitude = "latitude"
    static let receiverFirstname = "receiver_firstname"
    static let senderId = "sender_id"
    static let contactPic = "contact_pic"
    static let senderProfilePic = "sender_profile_pic"
    static let receiverProfilePic = "receiver_profile_pic"
    static let longitude = "longitude"
    static let message = "message"
    static let contactName = "contact_name"
    static let readStatus = "read_status"
    static let deleted = "deleted"
    static let receiverLastname = "receiver_lastname"
    static let receiverId = "receiver_id"
    static let createdDate = "created_date"
    static let contactId = "contact_id"
    static let senderLastname = "sender_lastname"
    static let messagetype = "messagetype"
    static let senderFirstname = "sender_firstname"
    static let messageId = "message_id"
  }

  // MARK: Properties

  public var media: String?
  public var contactNumber: String?
  public var latitude: String?
  public var receiverFirstname: String?
  public var senderId: Int?
  public var contactPic: String?
  public var senderProfilePic: String?
  public var receiverProfilePic: String?
  public var longitude: String?
  public var message: String?
  public var contactName: String?
  public var readStatus: Int?
  public var deleted: Int?
  public var receiverLastname: String?
  public var receiverId: Int?
  public var createdDate: String?
  public var contactId: String?
  public var senderLastname: String?
  public var messagetype: Int?
  public var senderFirstname: String?
  public var messageId: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    media = json[SerializationKeys.media].string
    contactNumber = json[SerializationKeys.contactNumber].string
    latitude = json[SerializationKeys.latitude].string
    receiverFirstname = json[SerializationKeys.receiverFirstname].string
    senderId = json[SerializationKeys.senderId].int
    contactPic = json[SerializationKeys.contactPic].string
    senderProfilePic = json[SerializationKeys.senderProfilePic].string
    receiverProfilePic = json[SerializationKeys.receiverProfilePic].string
    longitude = json[SerializationKeys.longitude].string
    message = json[SerializationKeys.message].string
    contactName = json[SerializationKeys.contactName].string
    readStatus = json[SerializationKeys.readStatus].int
    deleted = json[SerializationKeys.deleted].int
    receiverLastname = json[SerializationKeys.receiverLastname].string
    receiverId = json[SerializationKeys.receiverId].int
    createdDate = json[SerializationKeys.createdDate].string
    contactId = json[SerializationKeys.contactId].string
    senderLastname = json[SerializationKeys.senderLastname].string
    messagetype = json[SerializationKeys.messagetype].int
    senderFirstname = json[SerializationKeys.senderFirstname].string
    messageId = json[SerializationKeys.messageId].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = media { dictionary[SerializationKeys.media] = value }
    if let value = contactNumber { dictionary[SerializationKeys.contactNumber] = value }
    if let value = latitude { dictionary[SerializationKeys.latitude] = value }
    if let value = receiverFirstname { dictionary[SerializationKeys.receiverFirstname] = value }
    if let value = senderId { dictionary[SerializationKeys.senderId] = value }
    if let value = contactPic { dictionary[SerializationKeys.contactPic] = value }
    if let value = senderProfilePic { dictionary[SerializationKeys.senderProfilePic] = value }
    if let value = receiverProfilePic { dictionary[SerializationKeys.receiverProfilePic] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = contactName { dictionary[SerializationKeys.contactName] = value }
    if let value = readStatus { dictionary[SerializationKeys.readStatus] = value }
    if let value = deleted { dictionary[SerializationKeys.deleted] = value }
    if let value = receiverLastname { dictionary[SerializationKeys.receiverLastname] = value }
    if let value = receiverId { dictionary[SerializationKeys.receiverId] = value }
    if let value = createdDate { dictionary[SerializationKeys.createdDate] = value }
    if let value = contactId { dictionary[SerializationKeys.contactId] = value }
    if let value = senderLastname { dictionary[SerializationKeys.senderLastname] = value }
    if let value = messagetype { dictionary[SerializationKeys.messagetype] = value }
    if let value = senderFirstname { dictionary[SerializationKeys.senderFirstname] = value }
    if let value = messageId { dictionary[SerializationKeys.messageId] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.media = aDecoder.decodeObject(forKey: SerializationKeys.media) as? String
    self.contactNumber = aDecoder.decodeObject(forKey: SerializationKeys.contactNumber) as? String
    self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? String
    self.receiverFirstname = aDecoder.decodeObject(forKey: SerializationKeys.receiverFirstname) as? String
    self.senderId = aDecoder.decodeObject(forKey: SerializationKeys.senderId) as? Int
    self.contactPic = aDecoder.decodeObject(forKey: SerializationKeys.contactPic) as? String
    self.senderProfilePic = aDecoder.decodeObject(forKey: SerializationKeys.senderProfilePic) as? String
    self.receiverProfilePic = aDecoder.decodeObject(forKey: SerializationKeys.receiverProfilePic) as? String
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.contactName = aDecoder.decodeObject(forKey: SerializationKeys.contactName) as? String
    self.readStatus = aDecoder.decodeObject(forKey: SerializationKeys.readStatus) as? Int
    self.deleted = aDecoder.decodeObject(forKey: SerializationKeys.deleted) as? Int
    self.receiverLastname = aDecoder.decodeObject(forKey: SerializationKeys.receiverLastname) as? String
    self.receiverId = aDecoder.decodeObject(forKey: SerializationKeys.receiverId) as? Int
    self.createdDate = aDecoder.decodeObject(forKey: SerializationKeys.createdDate) as? String
    self.contactId = aDecoder.decodeObject(forKey: SerializationKeys.contactId) as? String
    self.senderLastname = aDecoder.decodeObject(forKey: SerializationKeys.senderLastname) as? String
    self.messagetype = aDecoder.decodeObject(forKey: SerializationKeys.messagetype) as? Int
    self.senderFirstname = aDecoder.decodeObject(forKey: SerializationKeys.senderFirstname) as? String
    self.messageId = aDecoder.decodeObject(forKey: SerializationKeys.messageId) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(media, forKey: SerializationKeys.media)
    aCoder.encode(contactNumber, forKey: SerializationKeys.contactNumber)
    aCoder.encode(latitude, forKey: SerializationKeys.latitude)
    aCoder.encode(receiverFirstname, forKey: SerializationKeys.receiverFirstname)
    aCoder.encode(senderId, forKey: SerializationKeys.senderId)
    aCoder.encode(contactPic, forKey: SerializationKeys.contactPic)
    aCoder.encode(senderProfilePic, forKey: SerializationKeys.senderProfilePic)
    aCoder.encode(receiverProfilePic, forKey: SerializationKeys.receiverProfilePic)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(contactName, forKey: SerializationKeys.contactName)
    aCoder.encode(readStatus, forKey: SerializationKeys.readStatus)
    aCoder.encode(deleted, forKey: SerializationKeys.deleted)
    aCoder.encode(receiverLastname, forKey: SerializationKeys.receiverLastname)
    aCoder.encode(receiverId, forKey: SerializationKeys.receiverId)
    aCoder.encode(createdDate, forKey: SerializationKeys.createdDate)
    aCoder.encode(contactId, forKey: SerializationKeys.contactId)
    aCoder.encode(senderLastname, forKey: SerializationKeys.senderLastname)
    aCoder.encode(messagetype, forKey: SerializationKeys.messagetype)
    aCoder.encode(senderFirstname, forKey: SerializationKeys.senderFirstname)
    aCoder.encode(messageId, forKey: SerializationKeys.messageId)
  }

}
