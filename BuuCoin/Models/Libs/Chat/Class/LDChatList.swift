//
//  LDChatList.swift
//
//  Created by Cears on 06/09/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class LDChatList: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let loginUserId = "login_user_id"
    static let profilePic = "profile_pic"
    static let chatId = "chatId"
    static let lastmessageTime = "lastmessage_time"
    static let firstname = "firstname"
    static let lastname = "lastname"
    static let chatUserId = "chat_user_id"
    static let lastmessage = "lastmessage"
  }

  // MARK: Properties
  public var loginUserId: Int?
  public var profilePic: String?
  public var chatId: Int?
  public var lastmessageTime: String?
  public var firstname: String?
  public var lastname: String?
  public var chatUserId: Int?
  public var lastmessage: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    loginUserId = json[SerializationKeys.loginUserId].int
    profilePic = json[SerializationKeys.profilePic].string
    chatId = json[SerializationKeys.chatId].int
    lastmessageTime = json[SerializationKeys.lastmessageTime].string
    firstname = json[SerializationKeys.firstname].string
    lastname = json[SerializationKeys.lastname].string
    chatUserId = json[SerializationKeys.chatUserId].int
    lastmessage = json[SerializationKeys.lastmessage].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = loginUserId { dictionary[SerializationKeys.loginUserId] = value }
    if let value = profilePic { dictionary[SerializationKeys.profilePic] = value }
    if let value = chatId { dictionary[SerializationKeys.chatId] = value }
    if let value = lastmessageTime { dictionary[SerializationKeys.lastmessageTime] = value }
    if let value = firstname { dictionary[SerializationKeys.firstname] = value }
    if let value = lastname { dictionary[SerializationKeys.lastname] = value }
    if let value = chatUserId { dictionary[SerializationKeys.chatUserId] = value }
    if let value = lastmessage { dictionary[SerializationKeys.lastmessage] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.loginUserId = aDecoder.decodeObject(forKey: SerializationKeys.loginUserId) as? Int
    self.profilePic = aDecoder.decodeObject(forKey: SerializationKeys.profilePic) as? String
    self.chatId = aDecoder.decodeObject(forKey: SerializationKeys.chatId) as? Int
    self.lastmessageTime = aDecoder.decodeObject(forKey: SerializationKeys.lastmessageTime) as? String
    self.firstname = aDecoder.decodeObject(forKey: SerializationKeys.firstname) as? String
    self.lastname = aDecoder.decodeObject(forKey: SerializationKeys.lastname) as? String
    self.chatUserId = aDecoder.decodeObject(forKey: SerializationKeys.chatUserId) as? Int
    self.lastmessage = aDecoder.decodeObject(forKey: SerializationKeys.lastmessage) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(loginUserId, forKey: SerializationKeys.loginUserId)
    aCoder.encode(profilePic, forKey: SerializationKeys.profilePic)
    aCoder.encode(chatId, forKey: SerializationKeys.chatId)
    aCoder.encode(lastmessageTime, forKey: SerializationKeys.lastmessageTime)
    aCoder.encode(firstname, forKey: SerializationKeys.firstname)
    aCoder.encode(lastname, forKey: SerializationKeys.lastname)
    aCoder.encode(chatUserId, forKey: SerializationKeys.chatUserId)
    aCoder.encode(lastmessage, forKey: SerializationKeys.lastmessage)
  }

}
