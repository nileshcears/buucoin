//
//  DBManager.swift
//  DoctorApp
//
//  Created by Cears on 11/08/18.
//  Copyright © 2018 Cears. All rights reserved.
//

import UIKit
import SwiftyJSON

class DBManager: NSObject {
    static let shared: DBManager = DBManager()
    let databaseFileName = APP_DATABASE
    var pathToDatabase: String!
    var database: FMDatabase!
    

    override init() {
        super.init()
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
    }
  
    func createDatabase()  {
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            if database != nil {
                // Open the database.
                if database.open() {
                    print("Database Path => \(pathToDatabase)")
                    self.ceateDatabaseTable()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
    }
    
    func ceateDatabaseTable()
    {
        let createTblDiscussion = "CREATE TABLE \(tbl_Discussion) (\(Chat_ID)   INTEGER PRIMARY KEY AUTOINCREMENT,\(Chat_User_ID)    INTEGER,\(Login_User_ID)    INTEGER,\(Profile_Pic)    TEXT,\(Firstname)    TEXT,\(Lastname)    TEXT,\(Lastmessage)    TEXT,\(Lastmessage_Time)    TEXT);"
        
        let createTblChat = "CREATE TABLE \(tbl_ChatData) (\(Message_ID)     INTEGER,\(Sender_ID)     INTEGER,\(Receiver_ID)     INTEGER,\(Sender_Firstname)     TEXT,\(Sender_Lastname)     TEXT,\(Sender_Profiel_Pic)     TEXT,\(Receiver_Firstname)     TEXT,\(Receiver_Lastname)     TEXT,\(Receiver_Profiel_Pic)     TEXT,\(Message_Type)     INTEGER,\(Message)     TEXT,\(Media)     TEXT,\(Latitude)     TEXT,\(Longitude)     TEXT,\(Contact_ID)     TEXT,\(Contact_Name)     TEXT,\(Contact_Pic)     TEXT,\(Contact_Number)     TEXT,\(Read_Status)   INTEGER,\(Deleted)      INTEGER,\(Created_Date)      TEXT,PRIMARY KEY(\(Message_ID)));"

        
        let queue = FMDatabaseQueue.init(path: pathToDatabase)
        queue?.inTransaction { database, rollback in
            do {
                try database.executeUpdate(createTblDiscussion, values: [1])
                try database.executeUpdate(createTblChat, values: [2])
            
            } catch {
                rollback.pointee = true
                print(error)
            }
        }
        database.close()
    }
    

    public func insertNewTextMessage(dicMessage:NSDictionary)
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil {
            // Open the database.
            if database.open() {
                print("Database Path => \(pathToDatabase)")
                let strField = "(\(Message_ID),\(Sender_ID),\(Receiver_ID),\(Sender_Firstname),\(Sender_Lastname),\(Sender_Profiel_Pic),\(Receiver_Firstname),\(Receiver_Lastname),\(Receiver_Profiel_Pic),\(Message_Type),\(Message),\(Media),\(Latitude),\(Longitude),\(Contact_ID),\(Contact_Name),\(Contact_Pic),\(Contact_Number),\(Read_Status),\(Deleted),\(Created_Date))"
                let  strValue = "'\(dicMessage.value(forKey: Message_ID) ?? "")','\(dicMessage.value(forKey: Sender_ID) ?? "")','\(dicMessage.value(forKey: Receiver_ID) ?? "")','\(dicMessage.value(forKey: Sender_Firstname) ?? "")','\(dicMessage.value(forKey: Sender_Lastname) ?? "")','\(dicMessage.value(forKey: Sender_Profiel_Pic) ?? "")','\(dicMessage.value(forKey: Receiver_Firstname) ?? "")','\(dicMessage.value(forKey: Receiver_Lastname) ?? "")','\(dicMessage.value(forKey: Receiver_Profiel_Pic) ?? "")','\(dicMessage.value(forKey: Message_Type) ?? "")','\(dicMessage.value(forKey: Message) ?? "")','\(dicMessage.value(forKey: Media) ?? "")','\(dicMessage.value(forKey: Latitude) ?? "")','\(dicMessage.value(forKey: Longitude) ?? "")','\(dicMessage.value(forKey: Contact_ID) ?? "")','\(dicMessage.value(forKey: Contact_Name) ?? "")','\(dicMessage.value(forKey: Contact_Pic) ?? "")','\(dicMessage.value(forKey: Contact_Number) ?? "")','\(dicMessage.value(forKey: Read_Status) ?? "")','\(dicMessage.value(forKey: Deleted) ?? "")','\(dicMessage.value(forKey: Created_Date) ?? "")'"
                
                let insertQry = "INSERT INTO \(tbl_ChatData) \(strField) VALUES (\(strValue))"
                let queue = FMDatabaseQueue.init(path: pathToDatabase)
                queue?.inTransaction { database, rollback in
                    do {
                        try database.executeUpdate(insertQry, values: [1])
                    } catch {
                        rollback.pointee = true
                        print(error)
                    }
                }
            }
            else {
                print("Could not open the database.")
            }
        }
        database.close()
       
        
        //Update Discusison
        if let msgCategory = dicMessage.value(forKey: "messageCategory") as? String
        {}
        else
        {
            let jsonData = JSON(dicMessage)
            let objMessage:ChatMessage = ChatMessage.init(json: jsonData)
            self.updateDiscussion(objMessage: objMessage)
        }
        
    }
    


    
    public func updateImageMessage(strMessageID:String,strPath:String)
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil {
            
            if database.open() {
                print("Database Path => \(pathToDatabase)")
                let queue = FMDatabaseQueue.init(path: pathToDatabase)
                queue?.inTransaction { database, rollback in
                    do {
                        let updateMessage = "UPDATE \(tbl_ChatData) set \(Media) = '\(strPath)'  WHERE \(Message_ID) = '\(strMessageID)'"
                        do {
                            try database.executeUpdate(updateMessage, values: nil)
                        }
                        catch {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
        }
        database.close()
    }
    

    public func updateDiscussion(objMessage:ChatMessage)
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil {
            let strAppUserID = "\(UserDefaults.standard.value(forKey: UserID) ?? "")"
            if database.open() {
                print("Database Path => \(pathToDatabase)")
                let queue = FMDatabaseQueue.init(path: pathToDatabase)
                queue?.inTransaction { database, rollback in
                    do {
                        let strQry = "SELECT count(*) as Record,\(Chat_ID) as ID FROM \(tbl_Discussion) WHERE  (\(Chat_User_ID) = \(objMessage.senderId!)  AND \(Login_User_ID) = \(objMessage.receiverId!)) OR (\(Chat_User_ID) = \(objMessage.receiverId!)  AND \(Login_User_ID) = \(objMessage.senderId!))"
                        let rs = try database.executeQuery(strQry, values: nil)
                     
                        while rs.next() {
                                let count = rs.int(forColumn: "Record")
                                if(count == 0)
                                {
                                    //INSERT NEW
                                    let strDiscussionField = "(\(Chat_User_ID),\(Login_User_ID),\(Profile_Pic),\(Firstname),\(Lastname),\(Lastmessage),\(Lastmessage_Time))"
                                    var strValueField = ""
                                   
                                    if(objMessage.senderId == Int("\(strAppUserID)"))
                                    {
                                        strValueField = "\(objMessage.receiverId!),\(objMessage.senderId!),'\(objMessage.receiverProfilePic ?? "")','\(objMessage.receiverFirstname ?? "")','\(objMessage.receiverLastname ?? "")','\(objMessage.message ?? "")','\(objMessage.createdDate  ?? "")'"
                                    }
                                    else
                                    {
                                         strValueField = "\(objMessage.receiverId!),\(objMessage.senderId!),'\(objMessage.senderProfilePic  ?? "")','\(objMessage.senderFirstname ?? "")','\(objMessage.senderLastname ?? "")','\(objMessage.message  ?? "")','\(objMessage.createdDate  ?? "")'"
                                    }
                                    let insertDiscussionQry = "INSERT INTO \(tbl_Discussion) \(strDiscussionField) VALUES (\(strValueField))"
                                    do {
                                        try database.executeUpdate(insertDiscussionQry, values: nil)
                                    }
                                    catch {
                                        print("Could not create table.")
                                        print(error.localizedDescription)
                                    }
                                }
                                else
                                {
                                    //UPDATE EXISTING
                                    let chatID = rs.int(forColumn: "ID")
                                    var updateDiscussion = ""
                                    if(objMessage.senderId == Int("\(strAppUserID)"))
                                    {
                                        updateDiscussion = "UPDATE \(tbl_Discussion) set \(Lastmessage) = '\(objMessage.message ?? "")' ,\(Lastmessage_Time) = '\(objMessage.createdDate ?? "")',\(Profile_Pic) = '\(objMessage.receiverProfilePic ?? "")',\(Firstname) = '\(objMessage.receiverFirstname ?? "")',\(Lastname) = '\(objMessage.receiverLastname ?? "")' WHERE \(Chat_ID) = '\(chatID)'"
                                    }
                                    else
                                    {
                                        updateDiscussion = "UPDATE \(tbl_Discussion) set \(Lastmessage) = '\(objMessage.message ?? "")' ,\(Lastmessage_Time) = '\(objMessage.createdDate ?? "")',\(Profile_Pic) = '\(objMessage.senderProfilePic ?? "")',\(Firstname) = '\(objMessage.senderFirstname ?? "")',\(Lastname) = '\(objMessage.senderLastname ?? "")' WHERE \(Chat_ID) = '\(chatID)'"
                                    }
                                    
                                    do {
                                        try database.executeUpdate(updateDiscussion, values: nil)
                                    }
                                    catch {
                                        print("Could not create table.")
                                        print(error.localizedDescription)
                                    }

                                }
                        }
                    } catch {
                        rollback.pointee = true
                        print(error)
                    }
                }
            }
        }
        database.close()
    }

    public func getAllMessaget(strSenderID:String,strReceiverID:String,complete:(_ isSuccess:Bool,_ strMessage:String?,_ arrMessage:NSMutableArray?) -> ())
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil {
            // Open the database.
            if database.open() {
                print("Database Path => \(pathToDatabase)")
                let queue = FMDatabaseQueue.init(path: pathToDatabase)
                queue?.inTransaction { database, rollback in
                    do {
                        let strQry = "SELECT * FROM \(tbl_ChatData) WHERE (\(Sender_ID) = '\(strSenderID)' AND \(Receiver_ID) = '\(strReceiverID)') OR (\(Sender_ID) = '\(strReceiverID)' AND \(Receiver_ID) = '\(strSenderID)') ORDER BY \(Created_Date)"
                        //print(strQry)
                        let rs = try database.executeQuery(strQry, values: nil)
                        let arrMessage = NSMutableArray()
                        while rs.next() { 
                            if let dicChatMessage = rs.resultDictionary as NSDictionary?
                            {
                                let jsonData = JSON(dicChatMessage)
                                let objData:LDMessage = LDMessage.init(json: jsonData)
                                arrMessage.add(objData)
                            }
                        }
                        complete(true,"Successfully retrive chat list...",arrMessage)
                    } catch {
                        rollback.pointee = true
                        complete(false,"Something went wrong to get chat list",nil)
                        print(error)
                    }
                }
            }
            else {
                complete(false,"Could not open the database.",nil)
            }
        }
        database.close()
    }
    
    public func getSingleMessage(messageID:String,complete:(_ isSuccess:Bool,_ strMessage:String?,_ objMessage:LDMessage?) -> ())
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil {
            // Open the database.
            if database.open() {
                print("Database Path => \(pathToDatabase)")
                let queue = FMDatabaseQueue.init(path: pathToDatabase)
                queue?.inTransaction { database, rollback in
                    do {
                        let strQry = "SELECT * FROM \(tbl_ChatData) WHERE  \(Message_ID) = '\(messageID)'"
                        let rs = try database.executeQuery(strQry, values: nil)
                        let arrMessage = NSMutableArray()
                        while rs.next() {
                            if let dicChatMessage = rs.resultDictionary as NSDictionary?
                            {
                                let jsonData = JSON(dicChatMessage)
                                let objData:LDMessage = LDMessage.init(json: jsonData)
                                arrMessage.add(objData)
                            }
                        }
                        complete(true,"Successfully retrive chat list...",(arrMessage[0] as! LDMessage))
                    } catch {
                        rollback.pointee = true
                        complete(false,"Something went wrong to get chat list",nil)
                        print(error)
                    }
                }
            }
            else {
                complete(false,"Could not open the database.",nil)
            }
        }
        database.close()
    }
    
    public func getAllNewMessaget(strSenderID:String,strReceiverID:String,lastMessageID:String,complete:(_ isSuccess:Bool,_ strMessage:String?,_ arrMessage:NSMutableArray?) -> ())
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil {
            // Open the database.
            if database.open() {
                print("Database Path => \(pathToDatabase)")
                let queue = FMDatabaseQueue.init(path: pathToDatabase)
                queue?.inTransaction { database, rollback in
                    do {
                        let strQry = "SELECT * FROM \(tbl_ChatData) WHERE ((\(Sender_ID) = '\(strSenderID)' AND \(Receiver_ID) = '\(strReceiverID)') OR (\(Sender_ID) = '\(strReceiverID)' AND \(Receiver_ID) = '\(strSenderID)')) AND (\(Message_ID) > '\(lastMessageID)')"
                        let rs = try database.executeQuery(strQry, values: nil)
                        let arrMessage = NSMutableArray()
                        while rs.next() {
                            if let dicChatMessage = rs.resultDictionary as NSDictionary?
                            {
                                let jsonData = JSON(dicChatMessage)
                                let objData:LDMessage = LDMessage.init(json: jsonData)
                                arrMessage.add(objData)
                            }
                        }
                        complete(true,"Successfully retrive chat list...",arrMessage)
                    } catch {
                        rollback.pointee = true
                        complete(false,"Something went wrong to get chat list",nil)
                        print(error)
                    }
                }
            }
            else {
                complete(false,"Could not open the database.",nil)
            }
        }
        database.close()
    }
    
    public func getAllChatList(complete:(_ isSuccess:Bool,_ strMessage:String?,_ arrChatList:NSMutableArray?) -> ())
    {
        database = FMDatabase(path: pathToDatabase!)
        if database != nil {
            // Open the database.
            if database.open() {
                print("Database Path => \(pathToDatabase)")
                let queue = FMDatabaseQueue.init(path: pathToDatabase)
                queue?.inTransaction { database, rollback in
                    do {
                        let rs = try database.executeQuery("SELECT * FROM \(tbl_Discussion)  WHERE \(Chat_User_ID) = '\(UserDefaults.standard.value(forKey: UserID) ?? "")' OR \(Login_User_ID) = '\(UserDefaults.standard.value(forKey: UserID) ?? "")' ORDER BY \(Lastmessage_Time) DESC", values: nil)
                        let arrChatList = NSMutableArray()
                        while rs.next() {
                            if let dicChatMessage = rs.resultDictionary as NSDictionary?
                            {
                                let jsonData = JSON(dicChatMessage)
                                let objData:LDChatList = LDChatList.init(json: jsonData)
                                arrChatList.add(objData)
                            }
                        }
                        complete(true,"Successfully retrive chat list...",arrChatList)
                    } catch {
                        rollback.pointee = true
                        complete(false,"Something went wrong to get chat list",nil)
                        print(error)
                    }
                }
            }
            else {
                complete(false,"Could not open the database.",nil)
            }
        }
        database.close()
    }

}
