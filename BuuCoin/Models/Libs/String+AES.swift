import Foundation
import CryptoSwift

extension String{
    func aesEncrypt() -> String {
        let data = self.data(using: .utf8)!
        let encrypted = try! AES(key: KEY1, iv: IV1, blockMode: .CBC).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        let hexData = (encryptedData.toHexString()).data(using: String.Encoding.utf8)
        let base64String: String = hexData!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return String(base64String)
    }
    
    func aesDecrypt() -> String {
        let data = self.base64Decoded()?.dataFromHexadecimalString()!
        let decrypted = try! AES(key: KEY, iv: IV, blockMode: .CBC).decrypt([UInt8](data!))
        let decryptedData = Data(decrypted)
        let result = String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
        return result
    }
    
    func dataFromHexadecimalString() -> Data? {
        var data = Data(capacity: self.count / 2)
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, options: [], range: NSMakeRange(0, self.count)) { match, flags, stop in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)
            data.append(num!)
        }
        return data
    }
    
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}

extension Data {
    
    public var bytes: Array<UInt8> {
        return Array(self)
    }
    
    public func toHexString() -> String {
        return self.bytes.toHexString()
    }
}


