//
//  AppDelegate.swift
//  BuuCoin
//
//  Created by iOS_1 on 21/08/18.
//  Copyright © 2018 iOS_1. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import XMPPFramework

@UIApplicationMain
class AppDelegate: UIResponder, UNUserNotificationCenterDelegate, UIApplicationDelegate {

    var window: UIWindow?
    var xmppController: XMPPController!
    var currentChatUserID:String = ""
    var isChatListScreen:Bool = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .lightContent
        IQKeyboardManager.shared.enable = true

        if (UserDefaults.standard.value(forKey: IS_LOGIN) == nil) {
            storeInUserDefault("0", forKey: IS_LOGIN)
        }
        if (UserDefaults.standard.value(forKey: IS_2FAAUTH) == nil) {
            storeInUserDefault("0", forKey: IS_2FAAUTH)
        }
        
        if UserDefaults.standard.value(forKey: IS_LOGIN) as! String == "1" {
            str2FAauthActive = UserDefaults.standard.object(forKey: IS_2FAAUTH) as! String
            strSecretPin = UserDefaults.standard.object(forKey: SECRETPIN) as! String
            strToken = UserDefaults.standard.object(forKey: LOGIN_TOKEN) as! String
            dictUserData = UserDefaults.standard.object(forKey: LOGIN_USER_DATA) as! NSDictionary
            /*
            let storyboard = UIStoryboard(name: SB_LS, bundle: nil)
            let navigation = storyboard.instantiateViewController(withIdentifier: "splashnav") as! UINavigationController
            window?.rootViewController = navigation*/
        }
        
        if (UserDefaults.standard.object(forKey: SECRETPIN) != nil) {
            strSecretPin = UserDefaults.standard.object(forKey: SECRETPIN) as! String
        }
        
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        application.registerForRemoteNotifications()
        
        connectToChat()
        
        //Create SQL DATABASE
        DBManager.shared.createDatabase()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        if UserDefaults.standard.value(forKey: IS_LOGIN) as! String == "1"{
            let storyboard = UIStoryboard(name: SB_LS, bundle: nil)
            let navigation = storyboard.instantiateViewController(withIdentifier: "passcodenav") as! UINavigationController
            window?.rootViewController = navigation
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        connectToChat()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate
{
    
    func connectToChat()
    {
        if ("\(UserDefaults.standard.value(forKey: IS_LOGIN) ?? "")" == "1") {
            do {
                try self.xmppController = XMPPController(hostName: CHATHOST,
                                                         userJIDString: "\(UserDefaults.standard.value(forKey: UserID) ?? "")@\(CHATHOST)",
                    password: "\(UserDefaults.standard.value(forKey: UserID) ?? "")")
                self.xmppController.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
                self.xmppController.connect()
            } catch {
                print("Something went wrong")
                //sender.showErrorMessage(message: "Something went wrong")
            }
        }
    }
    
}
//MARK:- Notification delegate methods
extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        saveInUserDefault(obj: deviceTokenString as AnyObject, key: UD_DeviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
        saveInUserDefault(obj: "simulatortoken" as AnyObject, key: UD_DeviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.noData)
    }
}

