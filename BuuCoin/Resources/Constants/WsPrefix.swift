//
//  Constants.swift
//
//  Created by C237 on 30/10/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

let GET = "GET"
let POST = "POST"
let MEDIA = "MEDIA"

let DEFAULT_TIMEOUT:TimeInterval = 60.0

let BASE_URL = "https://buucoins.io/api/"

//MARK: Response Keys
let kData = "data"
let kMessage = "message"
let kSuccess = "success"
let kToken = "token"
let kUserInfo = "userinfo"

//API Services
let APILogin = "auth/login"
let APISignUp = "auth/signup"
let APIVerify_OTP = "auth/verify-otp"
let APIForgotPassword = "auth/forgot-password"
let APIEditProfile = "user/update"




