//
//  KeyNamesConstants.swift
//  ELFramework
//
//  Created by Admin on 14/12/17.
//  Copyright © 2017 EL. All rights reserved.
//

import Foundation
import UIKit

struct SOCIAL {
    static let FACEBOOK = 0
    static let GOOGLE  = 1
    static let INSTAGRAM  = 2
    static let TWITTER = 3
}

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let loadercolor = [themeColor2, themeColor1]

//MARK:- APP NAME
public let App_Title :String = "BuuCoin"

public let IS_LOGIN = "islogin"
public let UserID = "UserID"

public let SECRETPIN = "secretpin"
public let LOGIN_TOKEN = "logintoken"
public let LOGIN_USER_DATA = "loginuserdata"
public let IS_2FAAUTH = "isauthactive"
public let AUTHFAILMSG = "Authentication failed!, Please Login Again"

public let IV = "dff7acda3c96fb78"
public let KEY = "aabd333347567f01"
public let IV1 = "2c87841d98309226"
public let KEY1 = "6d0b431391006459"

//MARK:- USER DEFAULTS KEY
let UD_DeviceToken:String = "UDDeviceToken"
let PLATFORM:String = "ios"

//MARK:- STORY BOARD NAMES
public let SB_MAIN: String = "Main"
public let SB_LS: String = "LoginSignUp"
public let SB_CHAT: String = "Chat"

//MARK:- COLOR NAMES
let APP_COLOR = Color_Hex(hex: "272D4A")
let themeColor1 = APP_COLOR
let themeColor2 = Color_Hex(hex: "9296B4")

//MARK:- CONTROLLERS ID
public let idRegisterVC = "RegisterVC"
public let idLoginVC = "LoginVC"
public let idPairWallet = "PairWallet"
public let idPasscode = "Passcode"
public let idSideMenuVC = "SideMenuVC"
public let idDashboardVC = "DashboardVC"
public let idSendReceiveVC = "SendReceiveVC"
public let idSendVC = "SendVC"
public let idReceiveVC = "ReceiveVC"
public let idTransactionVC = "TransactionVC"
public let idTransactionDetailsVC = "TransactionDetailsVC"
public let idSWRevealViewController = "SWRevealViewController"
public let idSellBuuVC = "SellBuuVC"
public let idBuyBuuVC = "BuyBuuVC"
public let idConversationVC = "ConversationVC"
public let idChatVC = "ChatVC"
public let idPayRequestVC = "PayRequestVC"
public let idLinkVC = "LinkVC"
public let idSupport = "Support"
public let idInviteFriendVC = "InviteFriendVC"
public let idLocalSearchVC = "LocalSearchVC"
public let idGlobalSearchVC = "GlobalSearchVC"

//MARK - MESSAGES
let ServerResponseError = "Server Response Error"
let RetryMessage = "Something went wrong please try again..."
let PasswordSent = "Password is sent to your email."
let InternetNotAvailable = "Internet connection appears to be offline."
let EnterEmail = "Email address is required."
let EnterFullname = "Full Name is required."
let EnterFname = "First Name is required."
let EnterLname = "Last Name is required."
let EnterAddress = "Address is required."
let EnterAmount = "Amount is required."
let EnterValidEmail = "Enter valid email address."
let EnterSecretPin = "Secret pin  is required."
let EnterPassword = "Password is required."
let EnterPasswordInvalid = "Enter valid password."
let EnterOLDPassword = "Old Password is required."
let EnterNEWPassword = "New Password is required."
let EnterMinLengthName = "Full name must be between 6 to 30 characters."
let EnterMinLengthPassword = "Minimum length of password is 8 character."
let EnterMinLengthMobile = "Minimum length of mobile is 6 character."
let EnterMinLengthPin = "Secret pin must be minimum 4 digit."
let EnterConfirmPassword = "Confirm password is required."
let EnterVerificationCode = "Verification code  is required."
let EnterValidVeriCode = "Enter valid verification code."
let PasswordMismatch = "Password Mismatch."
let PasswordChange = "Password Changed Successfully"
let EnterPhone = "Mobile number is required."
let EnterValidPhone = "Enter Valid mobile number."
let EnterUserName = "User name is required."
let EnterDOB = "Date of Birth is required."
let EnterVerification = "Verification Code is required."
let EnterCity = "City Name is required."
let PasswordRecovery = "Please check your mail box"
let EnterBetweenDate = "Please add valid age range."
let EnterGroupName = "Group name is required."
let EnterGroupDesc = "Description is required."
let EnterForgotPassMsg = "Username, E-mail or Mobile Number is required."
let ChooseCategory = "Choose category."
let ChooseSubCategory = "Choose subcategory."
let isFetchLocation = "isFetchLocation"
let EnterChannelName = "Channel name is required."
let EnterOTPCode = "Please enter OTP"
